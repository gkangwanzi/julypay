<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('api')->nullable();
            $table->string('public_key')->nullable();
            $table->string('callback')->nullable();
            $table->string('balance')->nullable();
            $table->string('username')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('business_name')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('twitter')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
};
