<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DepositOption;

class DepositOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deposit_options = [
            'Savings', 'Shares', 'Loan Repayment', 'Loan Application', 'MemberShip', 'Pass Book', 'Other Charges'
        ];

        foreach ($deposit_options as $option):
            DepositOption::firstOrCreate([
                'name' => $option
            ]);
        endforeach;
    }
}
