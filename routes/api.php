<?php

use App\Http\Controllers\api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('initiate_deposit', [ApiController::class, 'initiate_deposit']);
Route::post('withdraw', [ApiController::class, 'WithdrawFundsInitiationQuery']);
Route::post('getinstantnotification', [ApiController::class, 'getinstantnotification']);
Route::post('getfailurenotification', [ApiController::class, 'getfailurenotification']);
Route::post('request_relworx', [ApiController::class, 'request_relworx']);
Route::post('initiate_relworx', [ApiController::class, 'initiate_relworx']);
Route::post('send_money', [ApiController::class, 'send_money']);
Route::post('withdraw_relworx', [ApiController::class, 'withdraw_relworx']);
