<?php

use App\Http\Controllers\admin\AccountController;
use App\Http\Controllers\admin\ApiController;
use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\admin\DepositController;
use App\Http\Controllers\admin\WithdrawController;
use App\Http\Controllers\Auth\PermissionsController;
use App\Http\Controllers\Auth\RolesController;
use App\Http\Controllers\Auth\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes([
    'register' => true
]);

//Route::post('/login',[LoginController::class,'process_login'])->name('login');
//Route::post('/logout',[LoginController::class ,'logout'])->name('logout');

Route::get('/', ['middleware' => 'auth', 'uses' => 'DashboardController@index']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('users', UsersController::class);

    Route::group(['prefix' => 'transaction'], function () {
        Route::resource('deposits', DepositController::class);
        Route::resource('withdraws', WithdrawController::class);
    });

    Route::group(['prefix' => 'account'], function () {
        Route::get('general_setting', [AccountController::class,'general_setting'])->name('general_setting');
        Route::get('change_password', [AccountController::class,'change_password'])->name('change_password');
        Route::patch('modify_general/{id}', [AccountController::class,'modify_general'])->name('modify_general');
        Route::patch('modify_password/{id}', [AccountController::class,'modify_password'])->name('modify_password');
    });

    Route::group(['prefix' => 'api'], function () {
        Route::get('api_keys', [ApiController::class,'api_keys'])->name('api_keys');
        Route::get('call_back', [ApiController::class,'call_back'])->name('call_back');
        Route::patch('modify_key/{id}', [ApiController::class,'modify_key'])->name('modify_key');
        Route::patch('modify_callback/{id}', [ApiController::class,'modify_callback'])->name('modify_callback');
    });


    Route::resource('roles', RolesController::class);
    Route::resource('permissions', PermissionsController::class);

});
