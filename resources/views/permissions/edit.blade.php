@extends('content.main')

@section('title', 'Edit Produce')
@push('styles')
{{--    <link rel="stylesheet" href="{{ asset('assets/vendor/tom-select/dist/css/tom-select.bootstrap5.css') }}">--}}
@endpush
@section('content')
    <div class="content container-fluid">
      <!-- Page Header -->
      <div class="page-header">
        <div class="row align-items-end">
          <div class="col-sm mb-2 mb-sm-0">
                <h1 class="page-header-title"> Edit Produce</h1>
          </div>
          <!-- End Col -->

          <div class="col-auto">
                <a class="btn btn-primary" href="{{ url()->previous() }}" >
                    <i class="bi-backspace me-1"></i> Back
                </a>
          </div>
          <!-- End Col -->
        </div>
        <!-- End Row -->
      </div>
      <!-- End Page Header -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card card-info">
                    <div class="card-body">
                        <div class="bg-light p-4 rounded">
                            <h2>Edit permission</h2>
                            <div class="lead">
                                Editing permission.
                            </div>

                            <div class="container mt-4">

                                <form method="POST" action="{{ route('permissions.update', $permission->id) }}">
                                    @method('patch')
                                    @csrf
                                    <div class="mb-3">
                                        <label for="name" class="form-label">Name</label>
                                        <input value="{{ $permission->name }}"
                                               type="text"
                                               class="form-control"
                                               name="name"
                                               placeholder="Name" required>

                                        @if ($errors->has('name'))
                                            <span class="text-danger text-left">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>

                                    <button type="submit" class="btn btn-primary">Save permission</button>
                                    <a href="{{ route('permissions.index') }}" class="btn btn-default">Back</a>
                                </form>
                            </div>

                        </div>
{{--                        {!! Form::open(array('route' => ['produce.update', $stocking->id],'method'=>'PATCH')) !!}--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-lg-12 col-md-12">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label for="lastName">SELECT FARMER</label>--}}
{{--                                                <select name="farmer_name" class="form-control" required>--}}
{{--                                                    <option value="" selected>Please select farmer</option>--}}
{{--                                                    @foreach($farmer as $data)--}}
{{--                                                        @if($data->id == $stocking->farmer_name)--}}
{{--                                                            <option selected value={{ $data->id }}> {{ $data->full_names }} </option>--}}
{{--                                                        @else--}}
{{--                                                            <option value={{ $data->id }}> {{ $data->full_names }} </option>--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}

{{--                                            </div>--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">SELECT GROUP</label>--}}
{{--                                                <select name="group_name" class="form-control" required>--}}
{{--                                                    <option value="" selected>Please select farmer group</option>--}}
{{--                                                    @foreach($groups as $data)--}}
{{--                                                        @if($data->id == $stocking->group_name)--}}
{{--                                                            <option selected value={{ $data->id }}> {{ $data->group_names }} </option>--}}
{{--                                                        @else--}}
{{--                                                            <option value={{ $data->id }}> {{ $data->group_names }} </option>--}}
{{--                                                        @endif--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">PRODUCTION SEASON</label>--}}
{{--                                                <select name="season" class="form-control" required>--}}
{{--                                                    <option selected value={{ $stocking->season }}> {{ $stocking->season}} </option>--}}
{{--                                                    <option value="Beginning of year">Beginning of the year</option>--}}
{{--                                                    <option value="End of  year">End of the year</option>--}}
{{--                                                    <option value="Middle of year">Middle of the year</option>--}}
{{--                                                    <option value="Other season">Other season</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">CROP/ENTERPRISE</label>--}}
{{--                                                <input type="text" name="crop" value="{{ $stocking->crop }}" class="form-control" placeholder="Enter crop or enterprise" required="required">--}}
{{--                                            </div>--}}


{{--                                        </div>--}}
{{--                                        <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">Produce Details</label>--}}
{{--                                                <input type="text" name="landsize" value="{{ $stocking->landsize }}" class="form-control" placeholder="Enter Produce Details" required="required">--}}
{{--                                            </div>--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">Quantity Details</label>--}}
{{--                                                <input type="text" name="quantity" value="{{ $stocking->quantity }}" class="form-control" placeholder="Enter Quantity Details" required="required">--}}
{{--                                            </div>--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">Produce Valuation</label>--}}
{{--                                                <input type="text" name="cost" class="form-control" value="{{ $stocking->cost }}" placeholder="Enter Produce Valuation" required="required">--}}
{{--                                            </div>--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">Delivered By</label>--}}
{{--                                                <input type="text" name="deliveredby" value="{{ $stocking->deliveredby }}" class="form-control" placeholder="Enter Delivered By" required="required">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-12">--}}
{{--                                            <div class="mb-3 ">--}}
{{--                                                <button name="btn_submit" type="submit" class="btn btn-primary btn-block">Submit</button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        {!! Form::close() !!}--}}
                    </div>
                </div>
            </div>
        </div>



    </div>
    <!-- End Content -->
@endsection



@push('scripts')
    <script src="{{ asset('assets/vendor/tom-select/dist/js/tom-select.complete.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net.extensions/select/select.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/tom-select/dist/js/tom-select.complete.min.js') }}"></script>


  <script>
    (function() {
      window.onload = function () {

          HSCore.components.HSTomSelect.init('.js-select')
        // INITIALIZATION OF NAVBAR VERTICAL ASIDE
        // =======================================================
        new HSSideNav('.js-navbar-vertical-aside').init()

        // INITIALIZATION OF FORM SEARCH
        // =======================================================
        new HSFormSearch('.js-form-search')

        // INITIALIZATION OF BOOTSTRAP DROPDOWN
        // =======================================================
         HSBsDropdown.init()

        // INITIALIZATION OF NAV SCROLLER
        // =======================================================
        new HsNavScroller('.js-nav-scroller')

        // INITIALIZATION OF DROPZONE
        // =======================================================
         HSCore.components.HSDropzone.init('.js-dropzone')
      }
    })()
  </script>

  <script>
      (function () {
            // STYLE SWITCHER
            // =======================================================
            const $dropdownBtn = document.getElementById('selectThemeDropdown') // Dropdowon trigger
            const $variants = document.querySelectorAll(`[aria-labelledby="selectThemeDropdown"] [data-icon]`) // All items of the dropdown

            // Function to set active style in the dorpdown menu and set icon for dropdown trigger
            const setActiveStyle = function () {
              $variants.forEach($item => {
                if ($item.getAttribute('data-value') === HSThemeAppearance.getOriginalAppearance()) {
                  $dropdownBtn.innerHTML = `<i class="${$item.getAttribute('data-icon')}" />`
                  return $item.classList.add('active')
                }

                $item.classList.remove('active')
              })
            }

            // Add a click event to all items of the dropdown to set the style
            $variants.forEach(function ($item) {
              $item.addEventListener('click', function () {
                HSThemeAppearance.setAppearance($item.getAttribute('data-value'))
              })
            })

            // Call the setActiveStyle on load page
            setActiveStyle()

            // Add event listener on change style to call the setActiveStyle function
            window.addEventListener('on-hs-appearance-change', function () {
          setActiveStyle()
        })
      })()
    </script>
@endpush

