
@extends('api.index')
@section('report')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-info">
                <div class="card-body">
                    {!! Form::open(array('route' => ['modify_callback', $data->id],'method'=>'PATCH')) !!}
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">
                                    <div class="mb-3">
                                        <label>CallBack Url</label>
                                        <input type="text" class="form-control" value="{{ $data->callback }}" name="callback" placeholder="Enter Callback Url" required="required">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="mb-3"><br>
                            <button name="btn_submit" type="submit" class="btn btn-primary btn-block">Save Changes</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
