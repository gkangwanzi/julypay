
@extends('api.index')
@section('report')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-info">
                <div class="card-body">
                    {!! Form::open(array('route' => ['modify_key', $data->id],'method'=>'PATCH')) !!}
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">
                                    <div class="mb-3">
                                        <label>API Key</label>
                                        <input disabled type="text" class="form-control" value="{{ $data->api }}" name="username" placeholder="" required="required">
                                    </div>
                                    <div class="mb-3">
                                        <label>Public Key</label>
                                        <input disabled type="text" name="names" class="form-control" value="{{ $data->public_key }}" placeholder="" required="required">
                                    </div>

                                    <label><b>Note:</b> generating new keys affects your transaction approvals, after the change notify your admin to update your applications with the new keys.<br>Unless its the first time of generation.</label>

                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="mb-3"><br>
                            <button name="btn_submit" type="submit" class="btn btn-primary btn-block">Save Changes</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
