@php
    $farmer_count = 0;//App\Models\Farmer::count();
    $groupcount = 0;//App\Models\FarmerGroup::count();
    $imagecount = 0;
    $agroinputs = 0;//App\Models\Input::count();
@endphp

<div class="row">
    <div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
        <!-- Card -->
        <div class="card h-100 bg-soft-primary">
            <div class="card-body">
                <h6 class="card-subtitle mb-2">Wallet Balance</h6>

                <div class="row align-items-center gx-2">
                    <div class="col">
                        <span class="js-counter display-4 text-dark">{{ number_format($farmer_count) }}</span>
                    </div>
                    <!-- End Col -->

                    <div class="col-auto">
                                <span class="icon icon-primary icon-circle">
                                    <i class="bi-person"></i>
                                </span>
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Card -->
    </div>

    <div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
        <!-- Card -->
        <div class="card h-100 bg-soft-warning">
            <div class="card-body">
                <h6 class="card-subtitle mb-2">Pending Deposits</h6>

                <div class="row align-items-center gx-2">
                    <div class="col">
                        <span class="js-counter display-4 text-dark">{{ number_format($groupcount) }}</span>
                    </div>

                    <div class="col-auto">
                                <span class="icon icon-warning icon-circle">
                                    <i class="bi-hospital"></i>
                                </span>
                    </div>
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Card -->
    </div>

    <div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
        <!-- Card -->
        <div class="card h-100 bg-soft-info">
            <div class="card-body">
                <h6 class="card-subtitle mb-2">Successful Deposits</h6>

                <div class="row align-items-center gx-2">
                    <div class="col">
                        <span class="js-counter display-4 text-dark">{{ number_format($imagecount) }}</span>
                    </div>

                    <div class="col-auto">
                                <span class="icon icon-success icon-circle">
                                    <i class="bi-cloud-arrow-up"></i>
                                </span>
                    </div>
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Card -->
    </div>

    <div class="col-sm-6 col-lg-3 mb-3 mb-lg-5">
        <!-- Card -->
        <div class="card h-100 bg-soft-danger">
            <div class="card-body">
                <h6 class="card-subtitle mb-2">Withdrawn Amount</h6>

                <div class="row align-items-center gx-2">
                    <div class="col">
                        <span class="js-counter display-4 text-dark">{{ number_format($agroinputs) }}</span>
                    </div>

                    <div class="col-auto">
                                <span class="icon icon-danger icon-circle">
                                    <i class="bi-person-hearts"></i>
                                </span>
                    </div>
                </div>
                <!-- End Row -->
            </div>
        </div>
        <!-- End Card -->
    </div>
</div>
