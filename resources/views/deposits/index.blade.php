@extends('content.main')

@section('title', 'Deposits Home')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-sm mb-2 mb-sm-0">

                    <h1 class="page-header-title">Deposit Transactions</h1>
                </div>
                <!-- End Col -->

                <div class="col-auto">
                    <div class="col-md-12 mb-1">
                        <div class="d-flex justify-content-center pt-2">
                            <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#modalassign" data-bs-toggle="modal">Initial Deposit</a>
                        </div>
                    </div>
                </div>
                <!-- End Col -->
            </div>
            <!-- End Row -->
        </div>
        <!-- End Page Header -->

{{--        @include('content.statistic_banner')--}}

        <div class="row">

            <!-- User Content -->
            <div class="col-xl-12 col-lg-12 col-md-12 order-0 order-md-1">

                <div class="row">
                    <div class="col-sm-6 col-xl-3 mb-3 mb-xl-6">
                        <!-- Card -->
                        <div class="card card-sm h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <i class="bi-receipt nav-icon"></i>
                                            </div>

                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="mb-1">Wallet Balance</h5>
                                                <span class="d-block">UGX {{ number_format($user->balance) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>

                    <div class="col-sm-6 col-xl-3 mb-3 mb-xl-6">
                        <!-- Card -->
                        <div class="card card-sm h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <i class="bi-receipt nav-icon"></i>
                                            </div>

                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="mb-1">Pending Balance</h5>
                                                <span class="d-block">UGX 0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>

                    <div class="col-sm-6 col-xl-3 mb-3 mb-xl-6">
                        <!-- Card -->
                        <div class="card card-sm h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <!-- Media -->
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <i class="bi-bar-chart nav-icon"></i>
                                            </div>

                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="mb-1">Successful Deposits</h5>
                                                <span class="fs-5 text-warning">
                                                    <i class="tio-trending-up"></i> UGX 0
                                                </span>
                                            </div>
                                        </div>
                                        <!-- End Media -->
                                    </div>
                                    <!-- End Col -->

                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>

                    <div class="col-sm-6 col-xl-3 mb-3 mb-xl-6">
                        <!-- Card -->
                        <div class="card card-sm h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <!-- Media -->
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <i class="bi-bar-chart nav-icon"></i>
                                            </div>

                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="mb-1">Failed Transactions</h5>
                                                <span class="fs-5 text-warning">
                                                            <i class="tio-trending-up"></i> UGX 0
                                                        </span>
                                            </div>
                                        </div>
                                        <!-- End Media -->
                                    </div>
                                    <!-- End Col -->

                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                </div>
                <div class="card">
                    <!-- Header -->
                    <div class="card-header card-header-content-md-between">
                        <div class="mb-2 mb-md-0">
                            <form>
                                <!-- Search -->
                                <div class="input-group input-group-merge input-group-flush">
                                    <div class="input-group-prepend input-group-text">
                                        <i class="bi-search"></i>
                                    </div>
                                    <input id="datatableSearch" type="search" class="form-control" placeholder="Search users" aria-label="Search transactions ...">
                                </div>
                                <!-- End Search -->
                            </form>
                        </div>

                        <div class="d-grid d-sm-flex justify-content-md-end align-items-sm-center gap-2">
                            <!-- Datatable Info -->
                            <div id="datatableCounterInfo" style="display: none;">
                                <div class="d-flex align-items-center">
                            <span class="fs-5 me-3">
                              <span id="datatableCounter">0</span>
                              Selected
                            </span>
                                    <a class="btn btn-outline-danger btn-sm" href="javascript:;">
                                        <i class="bi-trash"></i> Delete
                                    </a>
                                </div>
                            </div>
                            <!-- End Datatable Info -->

                            <!-- Dropdown -->
                            <div class="dropdown">
                                <button type="button" class="btn btn-white btn-sm dropdown-toggle w-100" id="usersExportDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="bi-download me-2"></i> Export
                                </button>

                                <div class="dropdown-menu dropdown-menu-sm-end" aria-labelledby="usersExportDropdown">
                                    <span class="dropdown-header">Options</span>
                                    <a id="export-copy" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/illustrations/copy-icon.svg') }}" alt="Image Description">
                                        Copy
                                    </a>
                                    <a id="export-print" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/illustrations/print-icon.svg') }}" alt="Image Description">
                                        Print
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <span class="dropdown-header">Download options</span>
                                    <a id="export-excel" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/brands/excel-icon.svg') }}" alt="Image Description">
                                        Excel
                                    </a>
                                    <a id="export-csv" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/components/placeholder-csv-format.svg') }}" alt="Image Description">
                                        .CSV
                                    </a>
                                    <a id="export-pdf" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/brands/pdf-icon.svg') }}" alt="Image Description">
                                        PDF
                                    </a>
                                </div>
                            </div>
                            <!-- End Dropdown -->

                        </div>
                    </div>
                    <!-- End Header -->

                    <div class="table-responsive datatable-custom position-relative">
                        <table id="datatable" class="table table-lg table-borderless table-thead-bordered table-nowrap table-align-middle card-table" data-hs-datatables-options='{
                   "order": [],
                   "info": {
                     "totalQty": "#datatableWithPaginationInfoTotalQty"
                   },
                   "search": "#datatableSearch",
                   "entries": "#datatableEntries",
                   "pageLength": 5,
                   "isResponsive": false,
                   "isShowPaging": false,
                   "pagination": "datatablePagination"
                 }'>
                            <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th class="table-column-ps-0"><b style="color: black; font-size: larger">DATE</b></th>
                                <th class="table-column-ps-0"><b style="color: black; font-size: larger">PHONE</b></th>
                                <th><b style="color: black; font-size: larger">AMOUNT</b></th>
                                <th><b style="color: black; font-size: larger">TRANSACTION ID</b></th>
                                <th><b style="color: black; font-size: larger">STATUS</b></th>
                                <th><b style="color: black; font-size: larger">TRANSACTION REFERENCE</b></th>
{{--                                <th><b style="color: black; font-size: larger">Actions</b></th>--}}
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($member as $data)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td><b>{{ $data->created_at }}</b></td>
                                    <td>{{ $data->phone }}</td>
                                    <td>{{ $data->amount }}</td>
                                    <td>{{ $data->transaction_id }}</td>
                                    <td></td>
                                    <td>{{ $data->transaction_reference }}</td>
{{--                                    <td>--}}
{{--                                        <a class="btn btn-sm btn-outline-success waves-effect" href="{{ route('farmers.edit',$data->id) }}">Edit</a>--}}
{{--                                        {!! Form::open(['method' => 'DELETE','route' => ['farmers.destroy', $data->id],'style'=>'display:inline']) !!}--}}
{{--                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-outline-danger waves-effect']) !!}--}}
{{--                                        {!! Form::close() !!}--}}
{{--                                    </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                    <!-- Footer -->
                    <div class="card-footer">
                        <div class="row justify-content-center justify-content-sm-between align-items-sm-center">
                            <div class="col-sm mb-2 mb-sm-0">
                                <div class="d-flex justify-content-center justify-content-sm-start align-items-center">
                                    <span class="me-2">Showing:</span>

                                    <!-- Select -->
                                    <div class="tom-select-custom">
                                        <select id="datatableEntries" class="js-select form-select form-select-borderless w-auto" autocomplete="off" data-hs-tom-select-options='{"searchInDropdown": false,"hideSearch": true}'>
                                            <option value="10">10</option>
                                            <option value="15" selected>5</option>
                                            <option value="20">20</option>
                                        </select>
                                    </div>
                                    <!-- End Select -->

                                    <span class="text-secondary me-2">of</span>

                                    <!-- Pagination Quantity -->
                                    <span id="datatableWithPaginationInfoTotalQty"></span>
                                </div>
                            </div>
                            <!-- End Col -->

                            <div class="col-sm-auto">
                                <div class="d-flex justify-content-center justify-content-sm-end">
                                    <!-- Pagination -->
                                    <nav id="datatablePagination" aria-label="Activity pagination"></nav>
                                </div>
                            </div>
                            <!-- End Col -->
                        </div>
                        <!-- End Row -->
                    </div>
                    <!-- End Footer -->

                </div>
            </div>
        </div>



    </div>

@include('modals/deposit')
{{--@include('content/_partials/_modals/modal-edit-user')--}}
{{--@include('content/_partials/_modals/modal-upgrade-plan')--}}
@endsection

<!-- ========== END MAIN CONTENT ========== -->
@push('scripts')
    <!-- JS Plugins Init. -->
    <script>
        $(document).ready(function () {
            $('body').on('click', '#modalshellbutton', function () {
                var product_id = $(this).data('id');
                {{--$.get("{{ route('matches.index') }}" +'/' + product_id +'/edit', function (data) {--}}
                {{--    $('#insertInShell').html(data).show()--}}
                {{--    $('#modelshell').modal('show');--}}
                {{--})--}}
            });

            $('.show_confirm').click(function(event) {
                var form =  $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                Swal.fire({
                    title: 'Are you sure you want to delete this record?',
                    text: "If you delete this, it will be gone forever.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete!',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-outline-danger ms-1'
                    },
                    buttonsStyling: false
                }).then(function (result) {
                    if (result.value) {
                        form.submit();
                    }
                });
            });



        });

        function getMember(val){
            let url = "{{route('deposits.show', 7)}}"+val;
            document.location.href=url;
        }
        $(document).on('ready', function () {

            // INITIALIZATION OF DATATABLES
            // =======================================================
            HSCore.components.HSDatatables.init($('#datatable'), {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copy',
                        className: 'd-none'
                    },
                    {
                        extend: 'excel',
                        className: 'd-none'
                    },
                    {
                        extend: 'csv',
                        className: 'd-none'
                    },
                    {
                        extend: 'pdf',
                        className: 'd-none'
                    },
                    {
                        extend: 'print',
                        className: 'd-none'
                    },
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child input[type="checkbox"]',
                    classMap: {
                        checkAll: '#datatableCheckAll',
                        counter: '#datatableCounter',
                        counterInfo: '#datatableCounterInfo'
                    }
                },
                language: {
                    zeroRecords: `<div class="text-center p-4">
              <img class="mb-3" src="{{ asset("assets/svg/illustrations/oc-error.svg") }}" alt="Image Description" style="width: 10rem;" data-hs-theme-appearance="default">
              <img class="mb-3" src="{{ asset("assets/svg/illustrations-light/oc-error.svg") }}" alt="Image Description" style="width: 10rem;" data-hs-theme-appearance="dark">
            <p class="mb-0">No data to show</p>
            </div>`
                }
            });

            const datatable = HSCore.components.HSDatatables.getItem(0)

            $('#export-copy').click(function() {
                datatable.button('.buttons-copy').trigger()
            });

            $('#export-excel').click(function() {
                datatable.button('.buttons-excel').trigger()
            });

            $('#export-csv').click(function() {
                datatable.button('.buttons-csv').trigger()
            });

            $('#export-pdf').click(function() {
                datatable.button('.buttons-pdf').trigger()
            });

            $('#export-print').click(function() {
                datatable.button('.buttons-print').trigger()
            });

            $('.js-datatable-filter').on('change', function() {
                var $this = $(this),
                    elVal = $this.val(),
                    targetColumnIndex = $this.data('target-column-index');

                datatable.column(targetColumnIndex).search(elVal).draw();
            });
        });
    </script>

    <script>
        (function() {
            window.onload = function () {


                // INITIALIZATION OF NAVBAR VERTICAL ASIDE
                // =======================================================
                new HSSideNav('.js-navbar-vertical-aside').init()


                // INITIALIZATION OF FORM SEARCH
                // =======================================================
                new HSFormSearch('.js-form-search')


                // INITIALIZATION OF BOOTSTRAP DROPDOWN
                // =======================================================
                HSBsDropdown.init()


                // INITIALIZATION OF SELECT
                // =======================================================
                HSCore.components.HSTomSelect.init('.js-select')


                // INITIALIZATION OF FILE ATTACHMENT
                // =======================================================
                new HSFileAttach('.js-file-attach')


                // INITIALIZATION OF QUILLJS EDITOR
                // =======================================================
                HSCore.components.HSQuill.init('.js-quill')


                // INITIALIZATION OF DROPZONE
                // =======================================================
                HSCore.components.HSDropzone.init('.js-dropzone')


                // INITIALIZATION OF STEP FORM
                // =======================================================
                new HSStepForm('.js-step-form', {
                    finish: () => {
                        document.getElementById("createProjectStepFormProgress").style.display = 'none'
                        document.getElementById("createProjectStepFormContent").style.display = 'none'
                        document.getElementById("createProjectStepDetails").style.display = 'none'
                        document.getElementById("createProjectStepTerms").style.display = 'none'
                        document.getElementById("createProjectStepMembers").style.display = 'none'
                        document.getElementById("createProjectStepSuccessMessage").style.display = 'block'
                        const formContainer = document.getElementById('formContainer')
                    }
                })
            }
        })()
    </script>
@endpush

