@php use App\Models\DepositOption; @endphp
@extends('layouts/contentLayoutMaster')

@section('title', 'Deposits Home')

@section('vendor-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <section class="app-user-view-account">
        <div class="row">
            <!-- User Sidebar -->
            <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
                <!-- User Card -->
                <div class="card">
                    <div class="card-body">
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <img
                                    class="img-fluid rounded mt-3 mb-2"
                                    src="{{asset('images/portrait/small/avatar-s-2.jpg')}}"
                                    height="70"
                                    width="70"
                                    alt="User avatar"/>
                                <div class="user-info text-center">
                                    <h6>{{ $clients->account_names }}</h6>
                                    <span class="badge bg-light-success"><b>{{ $clients->account_number }}</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-around my-2 pt-75">
                            <div class="d-flex align-items-start me-2">
                    <span class="badge bg-light-primary p-75 rounded">
                        <i data-feather="check" class="font-medium-2"></i>
                    </span>
                                <div class="ms-75">
                                    <h6 class="mb-0">Savings Balance</h6>
                                    <h3><b>{{ number_format($clients->saving_balance) }}</b></h3>
                                </div>
                            </div>
                            <div class="d-flex align-items-start">
              <span class="badge bg-light-primary p-75 rounded">
                <i data-feather="briefcase" class="font-medium-2"></i>
              </span>
                                <div class="ms-75">
                                    <h6 class="mb-0">Outstanding Loan</h6>
                                    <h3><b>{{ number_format($clients->loan_balance + $clients->loan_interest) }}</b>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <select class="select2 form-select form-control" id="addMemberSelect"
                                onchange="getRequest(this.value)">
                            <option value="" label="select member...">select member...</option>
                            @foreach($clientlist as $data)
                                <option
                                    {{ $data->id == $selectedID ? 'selected="selected"' : '' }} value={{ $data->id }}>
                                    [{{ $data->account_number }}] {{ $data->account_names }}</option>
                            @endforeach
                        </select>
                        <br>
                        <h4 class="fw-bolder border-bottom pb-50 mb-1">Loan Details</h4>
                        <div class="info-container">
                            <ul class="list-unstyled">
                                <li class="mb-75">
                                    <span>Number of Loans taken so far:</span>
                                    <span class="fw-bolder me-25">0</span>
                                </li>
                                <li class="mb-75">
                                    <span>Loan Balance</span>
                                    <span class="fw-bolder me-25">0</span>
                                </li>
                                <li class="mb-75">
                                    <span class="">Interest Balance:</span>
                                    <span class="badge bg-light-success fw-bolder me-25">0</span>
                                </li>
                                <li class="mb-75">
                                    <span>Penalty Amount:</span>
                                    <span class="fw-bolder me-25">0</span>
                                </li>
                            </ul>
                        </div>
                        <br><br>
                        <h4 class="fw-bolder border-bottom pb-50 mb-1">Shares Details</h4>
                        <div class="info-container">
                            <ul class="list-unstyled">
                                <li class="mb-75">
                                    <span class="fw-bolder me-25">Shares Amount:</span>
                                    <span>{{ number_format($clients->shares_balance) }} | <b>No:</b> <span
                                            class="badge bg-light-success">{{ number_format($clients->number_of_shares) }}</span></span>
                                </li>
                            </ul>
                        </div>
                        <div class="d-flex justify-content-center pt-2">
                            <a hidden href="javascript:;" class="btn btn-primary me-1" data-bs-target="#editUser"
                               data-bs-toggle="modal">Edit</a>
                            <a hidden href="javascript:;" aria-hidden="true"
                               class="btn btn-outline-danger suspend-user">Suspend Account</a>
                        </div>
                    </div>
                </div>
                <!-- /User Card -->

            </div>
            <!--/ User Sidebar -->

            <!-- User Content -->
            <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
                <!-- User Pills -->
                <ul class="nav nav-pills mb-2">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('savings.index') }}">
                            <i data-feather="dollar-sign" class="font-medium-3 me-50"></i>
                            <span class="fw-bold">Deposit, Withdraw & Transfers</span></a
                        >
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{asset('app/user/view/security')}}">
                            <i data-feather="trending-up" class="font-medium-3 me-50"></i>
                            <span class="fw-bold">Till Transactions</span>
                        </a>
                    </li>
                </ul>
                <!--/ User Pills -->

                <!-- Invoice table -->
                <div class="card card-datatable">
                    <div class="row" style="padding: 10px">
                        <div class="col-md-6 mb-1">
                            <div class="d-flex justify-content-center pt-2">
                                <a id="editclientcontent" class="btn btn-success me-1" data-bs-target="#depositModal"
                                   data-bs-toggle="modal"
                                   data-attr="{{ route('deposit-options',['id'=> $selectedID]) }}">Deposit</a>
                                <a href="javascript:;" class="btn btn-warning me-1" data-bs-target="#withdrawModal"
                                   data-bs-toggle="modal" data-attr="{{ route('withdraws',['id'=> $selectedID]) }}">Withdraw</a>
                                <a href="javascript:;" class="btn btn-dark me-1" data-bs-target="#editUser"
                                   data-bs-toggle="modal">Transfers</a>
                            </div>
                        </div>
                        <div class="col-md-6 mb-1">
                            <label class="form-label" for="fp-range">Filter transactions with date range</label>
                            <input type="text" id="fp-range" class="form-control flatpickr-range"
                                   placeholder="YYYY-MM-DD to YYYY-MM-DD"/>
                        </div>
                    </div>
                    <div class="">
                        <table id="deposits" class="datatables-basic table table-sm">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th><b style="font-size: 12px">Withdrawls<br>DEBIT</b></th>
                                <th><b style="font-size: 12px">Deposits<br>CREDIT</b></th>
                                <th>Balance</th>
                                <th>Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($transactions as $key => $data)
                                @if($data->transaction_type == "1")
                                    @php
                                        $arr = DepositOption::whereIn('id', json_decode($data->deposit_categories, true))->pluck('name')->toArray();
                                        $arr_amount = json_decode($data->amount_breakdown, true);
                                        $deposited_items = join(', ', $arr)
                                    @endphp
                                    <tr>
                                        <td>{{\Carbon\Carbon::parse($data->created_at)->timezone('Africa/Nairobi')->toDayDateTimeString()}}</td>
                                        <td><b style="font-size: 12px">{{ $deposited_items }}</b><br>
                                            <small><b style="color: #08b44d">Deposited By: </b>{{ $data->handler }}
                                            </small></td>
                                        <td></td>
                                        <td>{{number_format($data->amount)}}</td>
                                        <td>{{number_format($data->saving)}}</td>
                                        <td>
                                            <button class="popper btn btn-icon btn-sm btn-flat-primary"
                                                    data-toggle="popover"
                                                    data-bs-trigger="hover" title="Deposit Break Down"><i
                                                    data-feather="eye"></i></button>
                                            <div style="display: none" class="popper-content hide">
                                                <ul class="list-group">
                                                    @foreach($arr as $i => $items)
                                                        <li class="list-group-item">{{ $items }} -
                                                            <b>{{ number_format($arr_amount[$i]) }}</b></li>
                                                    @endforeach
                                                    <li class="list-group-item"><b>Total:
                                                            &nbsp;&nbsp;&nbsp;&nbsp;{{ number_format($data->amount) }}</b>
                                                    </li>
                                                </ul>
                                            </div>
                                            <button type="button" class="btn btn-icon btn-sm btn-flat-success"><i
                                                    data-feather="printer"></i></button>
                                        </td>
                                    </tr>
                                @elseif($data->transaction_type == "2")
                                    <tr>
                                        <td>{{\Carbon\Carbon::parse($data->created_at)->timezone('Africa/Nairobi')->toDayDateTimeString()}}</td>
                                        <td><small><b style="color: #8c3233">Withdrawn By: </b>{{ $data->handler }}
                                            </small></td>
                                        <td>{{number_format($data->amount)}}</td>
                                        <td></td>
                                        <td>{{number_format($data->saving)}}</td>
                                        <td>
                                            <button type="button" class="btn btn-icon btn-sm btn-flat-success"><i
                                                    data-feather="printer"></i></button>
                                        </td>
                                    </tr>
                                @endif

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /Invoice table -->
            </div>
            <!--/ User Content -->
        </div>
    </section>

    @include('mfi/modals/modal-deposit')
    @include('mfi/modals/modal-withdraw')
    @include('content/_partials/_modals/modal-edit-user')
    @include('content/_partials/_modals/modal-upgrade-plan')
@endsection

@section('vendor-script')
    {{-- Vendor js files --}}
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/cleave.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/cleave/addons/cleave-phone.us.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    {{-- data table --}}
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>

@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/modal-edit-user.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-user-view-account.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>
    <script src="{{ asset('js/scripts/mfi/mfi-table-data.js') }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>
    <script src="{{ asset('js/scripts/mfi/table-modal-actions.js') }}"></script>
    <script>
        "use strict";

        function getRequest(val) {
            let url = "{{route('savings.show', 7)}}" + val;
            document.location.href = url;
        }

        function depositchoices(str, val) {
            if (document.getElementById('deptbox' + str).checked === true) {
                if (str === 2) {
                    document.getElementById('deptinput' + str).disabled = false;
                    document.getElementById("deptinput" + str).value = 0;
                } else {
                    if (val === 0) {
                        document.getElementById('deptinput' + str).disabled = false;
                    }
                    document.getElementById("deptinput" + str).value = val;
                }
                if (str === 3) {
                    let loan_amt = parseInt({{ $clients->loan_balance }})
                    if (loan_amt <= 0) {
                        // alert("Client Does not Have An Active Loan")
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'Client Does not Have An Active Loan',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });

                        document.getElementById('deptinput' + str).disabled = true;
                        document.getElementById('deptbox' + str).checked = false;
                        document.getElementById('deptinput' + str).value = "";
                    }
                }
                sumdepositamt(str);
            } else {
                sumdepositamt(str);
                document.getElementById('deptinput' + str).disabled = true;
                document.getElementById('deptinput' + str).value = "";
                document.getElementById('others' + str).innerHTML = "";
            }

        }

        function sumdepositamt(str) {
            if (document.getElementById('deptinput' + str).value == "") {
                document.getElementById('deptinput' + str).value = 0;
            }
            var trs = document.getElementById('deposittable').getElementsByTagName('tr');
            var tot = 0;
            for (var k = 1; k < trs.length; k++) {
                var tds = trs[k].getElementsByTagName('td');
                if (tds[0].getElementsByTagName('input')[0].checked == true) {
                    if (tds[2].getElementsByTagName('input')[0].value == "") {
                        tds[2].getElementsByTagName('input')[0].value = 0;
                    }
                    tot += parseInt(tds[2].getElementsByTagName('input')[0].value);
                }
            }
            document.getElementById('deposittotamt1').innerHTML = addCommas(tot);
            document.getElementById('deposittotamt').value = tot;

        }

        function addCommas(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        $(document).ready(function () {
            $(document).on('click', '#savebutdeposit', function (e) {
                e.preventDefault();
                var depcheckval = "1";
                if (depcheckval == "2") {
                    Swal.fire({
                        position: 'top-end',
                        type: 'warning',
                        title: 'YOU ARE UNDER A CLOSED COUNTER, CONTACT CHIEF CASHIER TO OPEN!',
                        showConfirmButton: !1,
                        timer: 1500,
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: !1
                    });
                } else {
                    if (document.getElementById('depositor').value == '') {
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'Enter Depositor and try again.',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
                    } else if (document.getElementById('deposittotamt').value == '' || document.getElementById('deposittotamt').value == '0') {
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'Provide deposit entries and try again.',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
                    } else {
                        Swal.fire({
                            title: 'Are you sure?',
                            text: "You want to make this deposit!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, deposit!',
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ms-1'
                            },
                            buttonsStyling: false
                        }).then(function (result) {
                            if (result.value) {
                                document.getElementById('savebutdeposit').disabled = true;
                                var deposit_category = [];
                                var deposit_amount = [];
                                var depositor = $("#depositor").val()
                                var token = $("#token").val()

                                var trs = document.getElementById('deposittable').getElementsByTagName('tr');
                                for (var k = 1; k < trs.length; k++) {
                                    var tds = trs[k].getElementsByTagName('td');
                                    if (tds[0].getElementsByTagName('input')[0].checked === true) {
                                        deposit_category.push(tds[0].getElementsByTagName('input')[0].value)
                                        deposit_amount.push(tds[2].getElementsByTagName('input')[0].value)
                                    }
                                }
                                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});

                                $.ajax({
                                    url: "{{route('savings.store')}}",
                                    method: 'POST',
                                    data: {
                                        _token: token,
                                        "id": '{{$selectedID}}',
                                        "deposit_category": deposit_category,
                                        "deposit_amount": deposit_amount,
                                        "depositor": depositor
                                    },
                                    success: function (data) {
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Deposited!',
                                            text: 'Successful Deposit Transaction !',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                        setInterval(function () {
                                            location.reload();
                                        }, 2000)

                                        // if (data.success == 0) { } else {}

                                    }
                                });
                            } else if (result.dismiss === Swal.DismissReason.cancel) {
                                Swal.fire({
                                    title: 'Cancelled',
                                    text: 'Cancelled successfully',
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            }
                        });

                    }
                }
            })
            $(document).on('click', "#savebutwithdraw", function (e) {
                e.preventDefault();
                var depcheckval = "1";

                if (document.getElementById('withdrawor').value == '') {
                    Swal.fire({
                        position: 'top-end',
                        type: 'warning',
                        title: 'Enter Withdrawor and try again.',
                        showConfirmButton: !1,
                        timer: 1500,
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: !1
                    });
                } else if (document.getElementById('withdrawamt').value == '' || document.getElementById('withdrawamt').value == '0') {
                    Swal.fire({
                        position: 'top-end',
                        type: 'warning',
                        title: 'Provide withdraw amount and try again.',
                        showConfirmButton: !1,
                        timer: 1500,
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: !1
                    });
                } else {
                    var saving_balance = parseFloat({{ $clients->saving_balance }});
                    var minimum_saving = parseFloat({{ App\Helpers\Helper::get_business_settings("min_saving") }});
                    var amount = parseFloat(document.getElementById("withdrawamt").value);
                    var balance = saving_balance - amount;
                    if (amount > saving_balance) {
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'INSUFFICIENT FUNDS ON YOU ACCOUNT!',
                            text: 'Enter Amount within range and try again.',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
                    } else if (balance < minimum_saving) {
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'YOU CAN NOT WITHDRAW MORE THAN MINIMUM SAVING BALANCE',
                            text: 'Enter Amount within range and try again.',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
                    } else {
                        Swal.fire({
                            title: 'Are you sure?',
                            text: "You want to make this withdraw!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, withdraw!',
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ms-1'
                            },
                            buttonsStyling: false
                        }).then(function (result) {
                            if (result.value) {
                                document.getElementById('savebutwithdraw').disabled = true;
                                var amount_withdrawn = $("#withdrawamt").val()
                                var withdrawor = $("#withdrawor").val()
                                var token = $("#token").val()

                                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});

                                $.ajax({
                                    url: "{{route('withdraws')}}",
                                    method: 'POST',
                                    data: {
                                        _token: token,
                                        "id": '{{$selectedID}}',
                                        "amount_withdrawn": amount_withdrawn,
                                        "withdrawor": withdrawor
                                    },
                                    success: function (data) {
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Withdrawn!',
                                            text: 'Successful Withdraw Transaction !',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                        setInterval(function () {
                                            location.reload();
                                        }, 2000)
                                    }
                                });
                            } else if (result.dismiss === Swal.DismissReason.cancel) {
                                Swal.fire({
                                    title: 'Cancelled',
                                    text: 'Cancelled successfully',
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            }
                        });
                    }
                }
            })
        });

    </script>
@endsection

