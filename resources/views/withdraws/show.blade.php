@extends('content.main')

@section('title', 'Deposits Home')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-sm mb-2 mb-sm-0">

                    <h1 class="page-header-title">Deposit & Withdraw Transactions</h1>
                </div>
                <!-- End Col -->

                <div class="col-auto">
                    <div class="col-md-12 mb-1">
                        <div class="d-flex justify-content-center pt-2">
                            <a href="javascript:;" class="btn btn-secondary me-1" >Transaction History</a>
                            <a href="javascript:;" class="btn btn-primary me-1">Cash Drawer</a>
                        </div>
                    </div>
                </div>
                <!-- End Col -->
            </div>
            <!-- End Row -->
        </div>
        <!-- End Page Header -->

{{--        @include('content.statistic_banner')--}}

        <div class="row">
            <!-- User Sidebar -->
            <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
                <!-- User Card -->
                <div class="card">
                    <div class="card-body">
                        <div class="user-avatar-section">
                            <div class="row">
                                <div class="col-sm-6 col-xl-5 mb-3 mb-xl-6">
                                    <!-- Card -->
                                    <div class="card card-sm h-100">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="d-flex">
                                                        <div class="flex-grow-1 ms-3">
                                                            <h5 class="mb-1">Profile Image</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex align-items-center flex-column">
                                                    <span class="avatar avatar-xl avatar-circle">
                                                        <img class="avatar-img" src="{{ asset('assets/img/160x160/img10.jpg') }}" alt="Image Description">
                                                    </span>
                                                </div>
                                            </div>
                                            <!-- End Row -->
                                        </div>
                                    </div>
                                    <!-- End Card -->
                                </div>

                                <div class="col-sm-6 col-xl-7 mb-3 mb-xl-6">
                                    <!-- Card -->
                                    <div class="card card-sm h-100">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0">
                                                            <i class="bi-people nav-icon"></i>
                                                        </div>
                                                        <div class="flex-grow-1 ms-3">
                                                            <h5 class="mb-1">More Details</h5>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <h4 class="fw-bolder border-bottom pb-50 mb-1">Mini Statistics</h4>
                                                    <div class="info-container">
                                                        <ul class="list-unstyled">
                                                            <li class="mb-75">
                                                                <span class="fw-bolder me-25">Loan Count:</span>
                                                                <span>0</span>
                                                            </li>
                                                            <li class="mb-75">
                                                                <span class="fw-bolder me-25">Penalty Amt:</span>
                                                                <span>0</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Row -->
                                        </div>
                                    </div>
                                    <!-- End Card -->
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-center pt-2">
                            <a id="editclientcontent" class="btn btn-success me-1" data-bs-target="#depositModal" data-bs-toggle="modal" data-attr="{{ route('deposit-options',['id'=> $memberId]) }}">Deposit</a>
                            <a href="javascript:;" class="btn btn-warning me-1" data-bs-target="#withdrawModal" data-bs-toggle="modal" data-attr="{{ route('withdraws',['id'=> $memberId]) }}">Withdraw</a>
                            <a href="javascript:;" class="btn btn-primary me-1" data-bs-target="#withdrawModal" data-bs-toggle="modal" data-attr="{{ route('withdraws',['id'=> $memberId]) }}">Non-Cash</a>
                        </div><br><br>
                        <h4 class="pb-50 mb-1">Select Member Account</h4>
                        <div class="mb-3" style="max-width: 40rem;">
                            <div class="tom-select-custom">
                                <select name="member_account" onchange="getMember(this.value)" class="js-select form-select" autocomplete="off"
                                        data-hs-tom-select-options='{"placeholder": "Select a member ..."}'>
                                    <option value="">Select a member ...</option>
                                    @foreach($clientlist as $data)
                                        @if($data->id == $memberId)
                                            <option selected value={{ $data->id }}>[{{ $data->account_number }}] {{ $data->account_names }} </option>
                                        @else
                                            <option value={{ $data->id }}>[{{ $data->account_number }}] {{ $data->account_names }} </option>
                                        @endif
                                     @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /User Card -->

            </div>
            <!--/ User Sidebar -->

            <!-- User Content -->
            <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
                <div class="row">
                    <div class="col-sm-6 col-xl-3 mb-3 mb-xl-6">
                        <!-- Card -->
                        <div class="card card-sm h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <i class="bi-receipt nav-icon"></i>
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="mb-1">Savings Balance</h5>
                                                <span class="d-block">UGX {{ number_format($clients->saving_balance) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>

                    <div class="col-sm-6 col-xl-3 mb-3 mb-xl-6">
                        <!-- Card -->
                        <div class="card card-sm h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <i class="bi-receipt nav-icon"></i>
                                            </div>

                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="mb-1">Shares Balance</h5>
                                                <span class="d-block">UGX {{ number_format($clients->shares_balance) }} | <b>No:</b> <span
                                                        class="badge bg-light-success">{{ number_format($clients->number_of_shares) }}</span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>

                    <div class="col-sm-6 col-xl-3 mb-3 mb-xl-6">
                        <!-- Card -->
                        <div class="card card-sm h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <!-- Media -->
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <i class="bi-bar-chart nav-icon"></i>
                                            </div>

                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="mb-1">Loan Balance</h5>
                                                <span class="fs-5 text-warning">
                                                            <i class="tio-trending-up"></i> UGX {{ number_format($clients->loan_balance) }}
                                                        </span>
                                            </div>
                                        </div>
                                        <!-- End Media -->
                                    </div>
                                    <!-- End Col -->

                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>

                    <div class="col-sm-6 col-xl-3 mb-3 mb-xl-6">
                        <!-- Card -->
                        <div class="card card-sm h-100">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <!-- Media -->
                                        <div class="d-flex">
                                            <div class="flex-shrink-0">
                                                <i class="bi-bar-chart nav-icon"></i>
                                            </div>

                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="mb-1">Interest On Loan</h5>
                                                <span class="fs-5 text-warning">
                                                            <i class="tio-trending-up"></i> UGX {{ number_format($clients->loan_interest) }}
                                                        </span>
                                            </div>
                                        </div>
                                        <!-- End Media -->
                                    </div>
                                    <!-- End Col -->

                                </div>
                                <!-- End Row -->
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                </div>
                <div class="card">
                    <!-- Header -->
                    <div class="card-header card-header-content-md-between">
                        <div class="mb-2 mb-md-0">
                            <form>
                                <!-- Search -->
                                <div class="input-group input-group-merge input-group-flush">
                                    <div class="input-group-prepend input-group-text">
                                        <i class="bi-search"></i>
                                    </div>
                                    <input id="datatableSearch" type="search" class="form-control" placeholder="Search users" aria-label="Search transactions ...">
                                </div>
                                <!-- End Search -->
                            </form>
                        </div>

                        <div class="d-grid d-sm-flex justify-content-md-end align-items-sm-center gap-2">
                            <!-- Datatable Info -->
                            <div id="datatableCounterInfo" style="display: none;">
                                <div class="d-flex align-items-center">
                            <span class="fs-5 me-3">
                              <span id="datatableCounter">0</span>
                              Selected
                            </span>
                                    <a class="btn btn-outline-danger btn-sm" href="javascript:;">
                                        <i class="bi-trash"></i> Delete
                                    </a>
                                </div>
                            </div>
                            <!-- End Datatable Info -->

                            <!-- Dropdown -->
                            <div class="dropdown">
                                <button type="button" class="btn btn-white btn-sm dropdown-toggle w-100" id="usersExportDropdown" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="bi-download me-2"></i> Export
                                </button>

                                <div class="dropdown-menu dropdown-menu-sm-end" aria-labelledby="usersExportDropdown">
                                    <span class="dropdown-header">Options</span>
                                    <a id="export-copy" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/illustrations/copy-icon.svg') }}" alt="Image Description">
                                        Copy
                                    </a>
                                    <a id="export-print" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/illustrations/print-icon.svg') }}" alt="Image Description">
                                        Print
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <span class="dropdown-header">Download options</span>
                                    <a id="export-excel" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/brands/excel-icon.svg') }}" alt="Image Description">
                                        Excel
                                    </a>
                                    <a id="export-csv" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/components/placeholder-csv-format.svg') }}" alt="Image Description">
                                        .CSV
                                    </a>
                                    <a id="export-pdf" class="dropdown-item" href="javascript:;">
                                        <img class="avatar avatar-xss avatar-4x3 me-2" src="{{ asset('assets/svg/brands/pdf-icon.svg') }}" alt="Image Description">
                                        PDF
                                    </a>
                                </div>
                            </div>
                            <!-- End Dropdown -->

                        </div>
                    </div>
                    <!-- End Header -->

                    <!-- Table -->

                    <div class="table-responsive datatable-custom position-relative">
                        <table id="datatable" class="table table-lg table-borderless table-thead-bordered table-nowrap table-align-middle card-table" data-hs-datatables-options='{
                   "order": [],
                   "info": {
                     "totalQty": "#datatableWithPaginationInfoTotalQty"
                   },
                   "search": "#datatableSearch",
                   "entries": "#datatableEntries",
                   "pageLength": 5,
                   "isResponsive": false,
                   "isShowPaging": false,
                   "pagination": "datatablePagination"
                 }'>
                            <thead class="thead-light">
                            <tr>
                                <th class="table-column-ps-0"><b style="color: black; font-size: larger">Date</b></th>
                                <th><b style="color: black; font-size: larger">Description</b></th>
                                <th><b style="color: black; font-size: larger">Withdraws</b></th>
                                <th><b style="color: black; font-size: larger">Deposits</b></th>
                                <th><b style="color: black; font-size: larger">Balance</b></th>
                                <th><b style="color: black; font-size: larger">Actions</b></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($transactions as $key => $data)
                                @if($data->transaction_type == "1")
                                    @php
                                        $arr = App\Models\DepositOption::whereIn('id', json_decode($data->deposit_categories, true))->pluck('name')->toArray();
                                        $arr_amount = json_decode($data->amount_breakdown, true);
                                        $deposited_items = join(', ', $arr)
                                    @endphp
                                    <tr>
                                        <td>{{\Carbon\Carbon::parse($data->created_at)->timezone('Africa/Nairobi')->toDayDateTimeString()}}</td>
                                        <td><b style="font-size: 12px">{{ $deposited_items }}</b><br>
                                            <small><b style="color: #08b44d">Deposited By: </b>{{ $data->handler }} </small>
                                        </td>
                                        <td></td>
                                        <td>{{number_format($data->amount)}}</td>
                                        <td>{{number_format($data->saving)}}</td>
                                        <td>
                                            <button class="popper btn btn-icon btn-sm btn-flat-primary"
                                                    data-toggle="popover"
                                                    data-bs-trigger="hover" title="Deposit Break Down"><i
                                                    class="bi-eye success"></i></button>
                                            <div style="display: none" class="popper-content hide">
                                                <ul class="list-group">
                                                    @foreach($arr as $i => $items)
                                                        <li class="list-group-item">{{ $items }} -
                                                            <b>{{ number_format($arr_amount[$i]) }}</b></li>
                                                    @endforeach
                                                    <li class="list-group-item"><b>Total:
                                                            &nbsp;&nbsp;&nbsp;&nbsp;{{ number_format($data->amount) }}</b>
                                                    </li>
                                                </ul>
                                            </div>
                                            <button type="button" class="btn btn-icon btn-sm btn-flat-success"><i
                                                    class="bi-printer"></i></button>
                                        </td>
                                    </tr>
                                @elseif($data->transaction_type == "2")
                                    <tr>
                                        <td>{{\Carbon\Carbon::parse($data->created_at)->timezone('Africa/Nairobi')->toDayDateTimeString()}}</td>
                                        <td><small><b style="color: #8c3233">Withdrawn By: </b>{{ $data->handler }}
                                            </small></td>
                                        <td>{{number_format($data->amount)}}</td>
                                        <td></td>
                                        <td>{{number_format($data->saving)}}</td>
                                        <td>
                                            <button type="button" class="btn btn-icon btn-sm btn-flat-success"><i
                                                    class="bi-printer"></i></button>
                                        </td>
                                    </tr>
                                @endif

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- End Table -->

                    <!-- Footer -->
                    <div class="card-footer">
                        <div class="row justify-content-center justify-content-sm-between align-items-sm-center">
                            <div class="col-sm mb-2 mb-sm-0">
                                <div class="d-flex justify-content-center justify-content-sm-start align-items-center">
                                    <span class="me-2">Showing:</span>

                                    <!-- Select -->
                                    <div class="tom-select-custom">
                                        <select id="datatableEntries" class="js-select form-select form-select-borderless w-auto" autocomplete="off" data-hs-tom-select-options='{"searchInDropdown": false,"hideSearch": true}'>
                                            <option value="10">10</option>
                                            <option value="15" selected>5</option>
                                            <option value="20">20</option>
                                        </select>
                                    </div>
                                    <!-- End Select -->

                                    <span class="text-secondary me-2">of</span>

                                    <!-- Pagination Quantity -->
                                    <span id="datatableWithPaginationInfoTotalQty"></span>
                                </div>
                            </div>
                            <!-- End Col -->

                            <div class="col-sm-auto">
                                <div class="d-flex justify-content-center justify-content-sm-end">
                                    <!-- Pagination -->
                                    <nav id="datatablePagination" aria-label="Activity pagination"></nav>
                                </div>
                            </div>
                            <!-- End Col -->
                        </div>
                        <!-- End Row -->
                    </div>
                    <!-- End Footer -->

                </div>
            </div>
        </div>



    </div>

    @include('modals/modal-deposit')
    @include('modals/modal-withdraw')
{{--@include('content/_partials/_modals/modal-edit-user')--}}
{{--@include('content/_partials/_modals/modal-upgrade-plan')--}}
@endsection

<!-- ========== END MAIN CONTENT ========== -->
@push('scripts')
    <!-- JS Plugins Init. -->
    <script>

        function getMember(val){
            let url = "{{route('deposits.show', 7)}}"+val;
            document.location.href=url;
        }

        $(document).ready(function () {
            $(document).on('click', '#editclientcontent', function(event) {
                event.preventDefault();
                let href = $(this).attr('data-attr');
                $.ajax({
                    url: href,
                    success: function(result) {
                        $('#editIndividualAccountModel').modal("show");
                        $('#edit-individual-account-modal-content').html(result).show();
                        $("#selectmembers").select2();
                    },
                    error: function(jqXHR, testStatus, error) {
                        console.log(error);
                        alert("Page " + href + " cannot open. Error:" + error);
                        $('#loader').hide();
                    },
                    timeout: 8000
                })
            });
        });

        function depositchoices(str, val) {
            if (document.getElementById('deptbox' + str).checked === true) {
                if (str === 2) {
                    document.getElementById('deptinput' + str).disabled = false;
                    document.getElementById("deptinput" + str).value = 0;
                } else {
                    if (val === 0) {
                        document.getElementById('deptinput' + str).disabled = false;
                    }
                    document.getElementById("deptinput" + str).value = val;
                }
                if (str === 3) {
                    let loan_amt = parseInt({{ $clients->loan_balance }})
                    if (loan_amt <= 0) {
                        // alert("Client Does not Have An Active Loan")
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'Client Does not Have An Active Loan',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
{{--                        {{ Alert::success('Success', 'Done!') }}--}}
{{--                        @if (\Session::has('success'))--}}
{{--                        @php alert()->success('Success', \Session::get('success')); @endphp--}}
{{--                        @endif--}}

                        document.getElementById('deptinput' + str).disabled = true;
                        document.getElementById('deptbox' + str).checked = false;
                        document.getElementById('deptinput' + str).value = "";
                    }
                }
                sumdepositamt(str);
            } else {
                sumdepositamt(str);
                document.getElementById('deptinput' + str).disabled = true;
                document.getElementById('deptinput' + str).value = "";
                document.getElementById('others' + str).innerHTML = "";
            }

        }

        function sumdepositamt(str) {
            if (document.getElementById('deptinput' + str).value == "") {
                document.getElementById('deptinput' + str).value = 0;
            }
            var trs = document.getElementById('deposittable').getElementsByTagName('tr');
            var tot = 0;
            for (var k = 1; k < trs.length; k++) {
                var tds = trs[k].getElementsByTagName('td');
                if (tds[0].getElementsByTagName('input')[0].checked == true) {
                    if (tds[2].getElementsByTagName('input')[0].value == "") {
                        tds[2].getElementsByTagName('input')[0].value = 0;
                    }
                    tot += parseInt(tds[2].getElementsByTagName('input')[0].value);
                }
            }
            document.getElementById('deposittotamt1').innerHTML = addCommas(tot);
            document.getElementById('deposittotamt').value = tot;

        }

        function addCommas(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        $(document).ready(function () {
            $(document).on('click', '#savebutdeposit', function (e) {
                e.preventDefault();
                var depcheckval = "1";
                if (depcheckval == "2") {
                    Swal.fire({
                        position: 'top-end',
                        type: 'warning',
                        title: 'YOU ARE UNDER A CLOSED COUNTER, CONTACT CHIEF CASHIER TO OPEN!',
                        showConfirmButton: !1,
                        timer: 1500,
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: !1
                    });
                } else {
                    if (document.getElementById('depositor').value == '') {
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'Enter Depositor and try again.',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
                    } else if (document.getElementById('deposittotamt').value == '' || document.getElementById('deposittotamt').value == '0') {
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'Provide deposit entries and try again.',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
                    } else {
                        Swal.fire({
                            title: 'Are you sure?',
                            text: "You want to make this deposit!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, deposit!',
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ms-1'
                            },
                            buttonsStyling: false
                        }).then(function (result) {
                            if (result.value) {
                                document.getElementById('savebutdeposit').disabled = true;
                                var deposit_category = [];
                                var deposit_amount = [];
                                var depositor = $("#depositor").val()
                                var token = $("#token").val()

                                var trs = document.getElementById('deposittable').getElementsByTagName('tr');
                                for (var k = 1; k < trs.length; k++) {
                                    var tds = trs[k].getElementsByTagName('td');
                                    if (tds[0].getElementsByTagName('input')[0].checked === true) {
                                        deposit_category.push(tds[0].getElementsByTagName('input')[0].value)
                                        deposit_amount.push(tds[2].getElementsByTagName('input')[0].value)
                                    }
                                }
                                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});

                                $.ajax({
                                    url: "{{route('deposits.store')}}",
                                    method: 'POST',
                                    data: {
                                        _token: token,
                                        "id": '{{$memberId}}',
                                        "deposit_category": deposit_category,
                                        "deposit_amount": deposit_amount,
                                        "depositor": depositor
                                    },
                                    success: function (data) {
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Deposited!',
                                            text: 'Successful Deposit Transaction !',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                        setInterval(function () {
                                            location.reload();
                                        }, 2000)
                                    }
                                });
                            } else if (result.dismiss === Swal.DismissReason.cancel) {
                                Swal.fire({
                                    title: 'Cancelled',
                                    text: 'Cancelled successfully',
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            }
                        });

                    }
                }
            })
            $(document).on('click', "#savebutwithdraw", function (e) {
                e.preventDefault();
                var depcheckval = "1";

                if (document.getElementById('withdrawor').value == '') {
                    Swal.fire({
                        position: 'top-end',
                        type: 'warning',
                        title: 'Enter Withdrawor and try again.',
                        showConfirmButton: !1,
                        timer: 1500,
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: !1
                    });
                } else if (document.getElementById('withdrawamt').value == '' || document.getElementById('withdrawamt').value == '0') {
                    Swal.fire({
                        position: 'top-end',
                        type: 'warning',
                        title: 'Provide withdraw amount and try again.',
                        showConfirmButton: !1,
                        timer: 1500,
                        confirmButtonClass: 'btn btn-primary',
                        buttonsStyling: !1
                    });
                } else {
                    var saving_balance = parseFloat({{ $clients->saving_balance }});
                    var minimum_saving = parseFloat({{ App\Helpers\Helper::get_business_settings("min_saving") }});
                    var amount = parseFloat(document.getElementById("withdrawamt").value);
                    var balance = saving_balance - amount;
                    if (amount > saving_balance) {
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'INSUFFICIENT FUNDS ON YOU ACCOUNT!',
                            text: 'Enter Amount within range and try again.',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
                    } else if (balance < minimum_saving) {
                        Swal.fire({
                            position: 'top-end',
                            type: 'warning',
                            title: 'YOU CAN NOT WITHDRAW MORE THAN MINIMUM SAVING BALANCE',
                            text: 'Enter Amount within range and try again.',
                            showConfirmButton: !1,
                            timer: 1500,
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: !1
                        });
                    } else {
                        Swal.fire({
                            title: 'Are you sure?',
                            text: "You want to make this withdraw!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, withdraw!',
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ms-1'
                            },
                            buttonsStyling: false
                        }).then(function (result) {
                            if (result.value) {
                                document.getElementById('savebutwithdraw').disabled = true;
                                var amount_withdrawn = $("#withdrawamt").val()
                                var withdrawor = $("#withdrawor").val()
                                var token = $("#token").val()

                                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}});

                                $.ajax({
                                    url: "{{route('withdraws')}}",
                                    method: 'POST',
                                    data: {
                                        _token: token,
                                        "id": '{{$memberId}}',
                                        "amount_withdrawn": amount_withdrawn,
                                        "withdrawor": withdrawor
                                    },
                                    success: function (data) {
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Withdrawn!',
                                            text: 'Successful Withdraw Transaction !',
                                            customClass: {
                                                confirmButton: 'btn btn-success'
                                            }
                                        });
                                        setInterval(function () {
                                            location.reload();
                                        }, 2000)
                                    }
                                });
                            } else if (result.dismiss === Swal.DismissReason.cancel) {
                                Swal.fire({
                                    title: 'Cancelled',
                                    text: 'Cancelled successfully',
                                    icon: 'error',
                                    customClass: {
                                        confirmButton: 'btn btn-success'
                                    }
                                });
                            }
                        });
                    }
                }
            })
        });

        $(document).on('ready', function () {
            // INITIALIZATION OF DATATABLES
            // =======================================================
            HSCore.components.HSDatatables.init($('#datatable'), {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copy',
                        className: 'd-none'
                    },
                    {
                        extend: 'excel',
                        className: 'd-none'
                    },
                    {
                        extend: 'csv',
                        className: 'd-none'
                    },
                    {
                        extend: 'pdf',
                        className: 'd-none'
                    },
                    {
                        extend: 'print',
                        className: 'd-none'
                    },
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child input[type="checkbox"]',
                    classMap: {
                        checkAll: '#datatableCheckAll',
                        counter: '#datatableCounter',
                        counterInfo: '#datatableCounterInfo'
                    }
                },
                language: {
                    zeroRecords: `<div class="text-center p-4">
              <img class="mb-3" src="{{ asset("assets/svg/illustrations/oc-error.svg") }}" alt="Image Description" style="width: 10rem;" data-hs-theme-appearance="default">
              <img class="mb-3" src="{{ asset("assets/svg/illustrations-light/oc-error.svg") }}" alt="Image Description" style="width: 10rem;" data-hs-theme-appearance="dark">
            <p class="mb-0">No data to show</p>
            </div>`
                }
            });

            const datatable = HSCore.components.HSDatatables.getItem(0)

            $('#export-copy').click(function() {
                datatable.button('.buttons-copy').trigger()
            });

            $('#export-excel').click(function() {
                datatable.button('.buttons-excel').trigger()
            });

            $('#export-csv').click(function() {
                datatable.button('.buttons-csv').trigger()
            });

            $('#export-pdf').click(function() {
                datatable.button('.buttons-pdf').trigger()
            });

            $('#export-print').click(function() {
                datatable.button('.buttons-print').trigger()
            });

            $('.js-datatable-filter').on('change', function() {
                var $this = $(this),
                    elVal = $this.val(),
                    targetColumnIndex = $this.data('target-column-index');

                datatable.column(targetColumnIndex).search(elVal).draw();
            });
        });
    </script>
    <!-- JS Plugins Init. -->
    <script>
        (function() {
            window.onload = function () {


                // INITIALIZATION OF NAVBAR VERTICAL ASIDE
                // =======================================================
                new HSSideNav('.js-navbar-vertical-aside').init()


                // INITIALIZATION OF FORM SEARCH
                // =======================================================
                new HSFormSearch('.js-form-search')


                // INITIALIZATION OF BOOTSTRAP DROPDOWN
                // =======================================================
                HSBsDropdown.init()


                // INITIALIZATION OF SELECT
                // =======================================================
                HSCore.components.HSTomSelect.init('.js-select')


                // INITIALIZATION OF FILE ATTACHMENT
                // =======================================================
                new HSFileAttach('.js-file-attach')


                // INITIALIZATION OF QUILLJS EDITOR
                // =======================================================
                HSCore.components.HSQuill.init('.js-quill')


                // INITIALIZATION OF DROPZONE
                // =======================================================
                HSCore.components.HSDropzone.init('.js-dropzone')


                // INITIALIZATION OF STEP FORM
                // =======================================================
                new HSStepForm('.js-step-form', {
                    finish: () => {
                        document.getElementById("createProjectStepFormProgress").style.display = 'none'
                        document.getElementById("createProjectStepFormContent").style.display = 'none'
                        document.getElementById("createProjectStepDetails").style.display = 'none'
                        document.getElementById("createProjectStepTerms").style.display = 'none'
                        document.getElementById("createProjectStepMembers").style.display = 'none'
                        document.getElementById("createProjectStepSuccessMessage").style.display = 'block'
                        const formContainer = document.getElementById('formContainer')
                    }
                })
            }
        })()
    </script>
@endpush

