<h2 class="text-center mb-2 pb-50" id="twoFactorAuthAppsTitle"><b>Deposit Options</b></h2>

<form>
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    <div class="col-12">
        <div class="row">
        <div class="col-md-6"><h2><b>Total Deposited Amount</b></h2></div>
        <div class="col-md-6"><h2><b id="deposittotamt1">0</b></h2></div>
        </div>
        <div class="table-responsive">
                <table class="table table-bordered table-responsive" id="deposittable">
                    <thead class="table-dark">
                        <tr >
                            <th>#</th>
                            <th>Description</th>
                            <th>Amount</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($depositoptions as $data)
                            @if ($data->id != '7')
                                <tr>
                                    <td width="1%"><input name="deposit_category[]" id="deptbox{{ $loop->index + 1 }}" onchange="depositchoices({{ $loop->index + 1 }}, {{$data->amount}})" value="{{$data->id}}" type="checkbox"></td>
                                    <td width="35%">{{$data->name}}</td>
                                    <td width="64%">
                                        <input name="deposit_amount[]" onkeyup="sumdepositamt({{ $loop->index + 1 }})" disabled style="width: 100%" type="number" id="deptinput{{ $loop->index + 1 }}" class="form-control">
                                        <div id="others{{ $loop->index + 1 }}"></div>
                                    </td>
                                    <td id="supvalues{{ $loop->index + 1 }}" hidden>{{ $data->id }}</td>
                                </tr>
                            @endif
                        @endforeach

                    </tbody>
                </table>
                <input id="deposittotamt" hidden>

        </div>
    </div>

    <div class="col-12">
        <label class="form-label" for="modalEditUserFirstName">Provide Depositor</label>
        <input value="" type="text"id="depositor" name="depositor" class="form-control" placeholder="Deposited By" value="" data-msg="Deposited By" />
    </div>

    <div class="col-12 text-center mt-2 pt-50">
        <button id="savebutdeposit" type="submit" class="btn btn-success me-1">Submit Deposit</button>
        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
            Discard
        </button>
    </div>
</form>

