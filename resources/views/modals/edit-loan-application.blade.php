<h1 class="text-center mb-2 pb-50" id="twoFactorAuthAppsTitle">Loan Application Details</h1>
{{--{{ route('clients.update', ['client' => $loan->id]) }}--}}
<form id="editUservForm" class="row gy-1 pt-75" method="POST" action="{{ route('requests.update', ['request' => $loans->id]) }}">
    {{ csrf_field() }}
    @csrf
    @method("PUT")
    <h4>Account Detail: <b>{{ $loans->client_info->account_names }}</b> ({{ $loans->client_info->account_number }})</h4>
    <h4>Borrowing Details</h4>
    <input hidden="" type="text" name="account_type" value="4" />

    <div class="col-12">
        <label class="form-label" for="modalEditUserFirstName">Business Details of the Applicant</label>
        <input value="{{ $loans->loan_application_data->type_of_business }}" type="text" id="modalFirstName" name="business_details" class="form-control" placeholder="Business Details of the Applicant" data-msg="Please enter your first name" />
    </div>
    <div class="col-12">
        <label class="form-label" for="modalEditUserLastName">Purpose for borrowing</label>
        <input value="{{ $loans->loan_application_data->purpose_of_loan }}" type="text" id="modalEditUserFirstName" name="purpose_of_business" class="form-control" placeholder="Purpose for borrowing" data-msg="Please enter your first name" />
    </div>

    <h4>Loan Details</h4>


    <div class="col-12 col-md-6">
        <label class="form-label" for="modalEditUserLastName">Type of Loan</label>
       <select name="type_of_loan" class="form-control" id="">
            @foreach($loans_types as $data)
                <option  value={{ $data->id == $loans->loan_application_data->type_of_business ? $loans->loan_application_data->type_of_business  : $data->id }}> {{ $data->id == $loans->loan_application_data->type_of_business ? $data->name : $data->name }} </option>
            @endforeach

        </select>
    </div>

    <div class="col-12 col-md-6">
        <label class="form-label" for="modalEditUserFirstName">Period Category (Daily, Weekly, Monthly)</label>
        <select id="" name="period_type" class="form-select" aria-label="Default select example">
            <option value="{{ $loans->loan_type }}">{{ $loans->loan_type }}</option>
            <option value="Daily">Daily</option>
            <option value="Weekly">Weekly</option>
            <option value="Monthly">Monthly</option>
        </select>
    </div>

    <div class="col-12 col-md-6">
        <label class="form-label" for="modalEditUserFirstName">Amount Requested</label>
        <input value="{{ $loans->amount_requested }}" type="number" id="" name="amount_requested" class="form-control" placeholder="Amount Requested" data-msg="Please enter your first name" />
    </div>
    <div class="col-12 col-md-6">
        <label class="form-label" for="modalEditUserLastName">Duration</label>
        <input value="{{ $loans->loan_period }}" type="number" id="" name="duration" class="form-control" placeholder="Duration" data-msg="Please enter your first name" />
    </div>

    <div class="col-12">
        <label class="form-label" for="modalEditUserLastName">Disbursement date</label>
        <input value={{ $loans->requested_date }}"" type="date" id="" name="disburse_date" class="form-control" placeholder="Disbursement date" data-msg="Please enter your first name" />
    </div>


    <h4>Guarantor Details</h4>

    <div class="col-12 col-md-6">
        <label class="form-label" for="modalEditUserLastName">Guarantor 1</label>
        <input value="{{ $loans->loan_application_data->guarantor_one }}" type="text" id="" name="guarantor1" class="form-control" placeholder="Guarantor 1" data-msg="Please enter your first name" />
    </div>

    <div class="col-12 col-md-6">
        <label class="form-label" for="modalEditUserFirstName">Guarantor 2</label>
        <input value="{{ $loans->loan_application_data->guarantor_two }}" type="text" id="" name="guarantor2" class="form-control" placeholder="Guarantor 2" data-msg="Please enter your first name" />
    </div>

    <div class="col-12 text-center mt-2 pt-50">
        <button type="submit" class="btn btn-primary me-1">Submit</button>
        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">Discard</button>
    </div>
</form>
