<div class="modal fade" id="modalassign" tabindex="-1" aria-labelledby="twoFactorAuthAppsTitle" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg two-factor-auth-apps modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadFilesModalLabel">Deposit</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="editUservForm" class="row gy-1 pt-75" method="POST" action="{{ route("deposits.store") }}">
                <div class="modal-body pb-5 px-sm-5 mx-50" >

                    {{ csrf_field() }}
                    @csrf
                    <h4>Initiate Deposit</h4>


                    <div class="mb-3">
                        <label class="form-label" for="modalEditUserFirstName">Phone Number</label>
                        <input value="" type="text" id="modalFirstName" name="phone" class="form-control" placeholder="Phone Number e.g 256771234567" data-msg="Please country" />
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="modalEditUserFirstName">Amount</label>
                        <input value="" type="number" id="modalFirstName" name="amount" class="form-control" placeholder="Amount" data-msg="Please country" />
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Initiate</button>
                </div>
            </form>
        </div>
    </div>
</div>



