<!-- edit individual apps modal -->
<div class="modal fade" id="depositModal" tabindex="-1" aria-labelledby="twoFactorAuthAppsTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg two-factor-auth-apps modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pb-5 px-sm-5 mx-50" id="edit-individual-account-modal-content">


      </div>
    </div>
  </div>
</div>

