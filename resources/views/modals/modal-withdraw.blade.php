<!-- edit individual apps modal -->

<div class="modal fade" id="withdrawModal" tabindex="-1" aria-labelledby="twoFactorAuthAppsTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm two-factor-auth-apps modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pb-5 px-sm-5 mx-50" id="edit-individual-account-modal-content">

          <h2 class="text-center mb-2 pb-50" id="twoFactorAuthAppsTitle"><b>Withdraw Funds</b></h2>
          <center><h2 class="modal-title" id="exampleModalLabel"><span id="accountbal">Bal: UGX {{ number_format($user->balance) }}</span></h2></center>
          <label class="form-label" for="basic-icon-default-fullname">Withdraw:</label>
          <h4 class="modal-title" id="">Charge: <span id="chargeamount">0</span></h4>
          <h4 class="modal-title" id="">Total: <span id="totalamount">0</span></h4>

          <form method="POST" action="{{ route("withdraws.store") }}">
              {{ csrf_field() }}
              @csrf

              <div class="row">
                  <div class="col-12">
                      <div class="mb-1">
                          <label class="form-label" for="basic-icon-default-fullname">Withdraw Amount</label>
                          <input onkeyup="generateWithdrawFees()" type="number" name="amount" class="form-control dt-full-name" id="amountwithdraw" placeholder="Amount e.g 1000" aria-label="Amount e.g 1000" />
                      </div>
                  </div>

                  <div class="col-12">
                      <div class="mb-1">
                          <label class="form-label" for="basic-icon-default-post">Mobile Money</label>
                          <input type="tel" id="mobilenumber" name="phone" class="form-control dt-post" placeholder="Mobile Money Number e.g +256772000000" aria-label="Mobile Money Number e.g 256772000000" />
                      </div>
                  </div>

                  <div class="col-12 text-center mt-2 pt-50">
                      <button id="savebutwithdraw" type="submit" class="btn btn-warning me-1">Submit Withdraw</button>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>
</div>

