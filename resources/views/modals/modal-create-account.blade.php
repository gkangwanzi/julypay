<!-- two factor auth modal -->
<div class="modal fade" id="twoFactorAuthModal" tabindex="-1" aria-labelledby="twoFactorAuthTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg two-factor-auth  modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pb-5 px-sm-5 mx-50">
        <h1 class="text-center mb-1" id="twoFactorAuthTitle">Account Opening Category</h1>
        <p class="text-center mb-3">
          After selecting the preferred account choice
          <br />
          Tap on continue to register the account details
        </p>

        <div class="custom-options-checkable">
          <input class="custom-option-item-check" type="radio" name="twoFactorAuthRadio" id="twoFactorAuthIndividual" value="acc_ind" checked/>
          <label for="twoFactorAuthIndividual" class="custom-option-item d-flex align-items-center flex-column flex-sm-row px-3 py-2 mb-2">
            <span><i data-feather="user" class="font-large-2 me-sm-2 mb-2 mb-sm-0"></i></span>
            <span>
              <span class="custom-option-item-title h3">Individual Account</span>
              <span class="d-block mt-75">For Adults and Minor Accounts as well</span>
            </span>
          </label>

          <input class="custom-option-item-check" type="radio" name="twoFactorAuthRadio" value="acc_joint" id="twoFactorAuthJoint"/>
          <label for="twoFactorAuthJoint" class="custom-option-item d-flex align-items-center flex-column flex-sm-row px-3 py-2">
            <span><i data-feather="users" class="font-large-2 me-sm-2 mb-2 mb-sm-0"></i></span>
            <span>
              <span class="custom-option-item-title h3">Joint Account</span>
              <span class="d-block mt-75" >This needs two account holders</span>
            </span>
          </label>
          <br>
          <input class="custom-option-item-check" type="radio" name="twoFactorAuthRadio" value="acc_group" id="twoFactorAuthGroup"/>
          <label for="twoFactorAuthGroup" class="custom-option-item d-flex align-items-center flex-column flex-sm-row px-3 py-2">
            <span><i data-feather="users" class="font-large-2 me-sm-2 mb-2 mb-sm-0"></i></span>
            <span>
              <span class="custom-option-item-title h3">Group Account</span>
              <span class="d-block mt-75" >This has limitless group members</span>
            </span>
          </label>
          <br>
          <input class="custom-option-item-check" type="radio" name="twoFactorAuthRadio" value="acc_bus" id="twoFactorAuthBusiness"/>
          <label for="twoFactorAuthBusiness" class="custom-option-item d-flex align-items-center flex-column flex-sm-row px-3 py-2">
            <span><i data-feather="home" class="font-large-2 me-sm-2 mb-2 mb-sm-0"></i></span>
            <span>
              <span class="custom-option-item-title h3">Business Account</span>
              <span class="d-block mt-75" >All kinds of business</span>
            </span>
          </label>
        </div>

        <button id="nextStepAuth" class="btn btn-primary float-end mt-3">
          <span class="me-50">Continue</span>
          <i data-feather="chevron-right"></i>
        </button>
      </div>
    </div>
  </div>
</div>
<!-- / two factor auth modal -->

<!-- add individual apps modal -->
<div class="modal fade" id="twoFactorAuthAppsModal" tabindex="-1" aria-labelledby="twoFactorAuthAppsTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg two-factor-auth-apps modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pb-5 px-sm-5 mx-50">
        <h1 class="text-center mb-2 pb-50" id="twoFactorAuthAppsTitle">Individual Account Opening</h1>

        <h4>Personal Information</h4>

        <form id="editUserForm" class="row gy-1 pt-75" method="POST" action="{{ route('customers.store') }}">
            {{ csrf_field() }}
            <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditUserFirstName">First Name</label>
                <input type="text"id="modalFirstName" name="first_name" class="form-control" placeholder="First Name" value="" data-msg="Please enter your first name" />
                <input hidden="" type="text" name="account_type" value="1" />
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditUserLastName">Last Name</label>
                <input type="text"id="modalLastName" name="last_name" class="form-control" placeholder="Last Name" value="" data-msg="Please enter your first name" />
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditUserFirstName">Gender</label>
                <select id="modalEditUserStatus" name="gender" class="form-select" aria-label="Default select example">
                    <option selected>Gender</option>
                    <option value="Female">Female</option>
                    <option value="Male">Male</option>
                </select>
            </div>
            <div class="col-12 col-md-6">
              <label class="form-label" for="modalEditUserLastName">Date of Birth</label>
               <input type="date" id="modalEditUserFirstName" name="date_of_birth" class="form-control" placeholder="Date of Birth" value="" data-msg="Please enter your first name" />
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditUserFirstName">Marital Status</label>
                <select id="modalEditUserStatus" name="marital_status" class="form-select" aria-label="Default select example">
                    <option selected>Marital Status</option>
                    <option value="Married">Married</option>
                    <option value="Single">Single</option>
                </select>
            </div>
            <div class="col-12 col-md-6">
              <label class="form-label" for="modalEditUserLastName">Occupation</label>
               <input type="text"id="modalEditUserFirstName" name="occupation" class="form-control" placeholder="Occupation" value="" data-msg="Please enter your first name" />
            </div>

            <div class="col-12 col-md-6">
              <label class="form-label" for="modalEditUserFirstName">Nationality</label>
              <input type="text"id="modalEditUserFirstName" name="nationality" class="form-control" placeholder="Nationality" value="" data-msg="Please enter your first name" />
            </div>
            <div class="col-12 col-md-6">
              <label class="form-label" for="modalEditUserLastName">National ID/Passport</label>
               <input type="text"id="modalEditUserFirstName" name="national_id" class="form-control" placeholder="Nationa ID/Passport" value="" data-msg="Please enter your first name" />
            </div>

            <div class="col-12 col-md-6">
              <label class="form-label" for="modalEditUserFirstName">Mobile Number</label>
              <input type="text"id="modalEditUserFirstName" name="mobile_number" class="form-control phone-number-mask" placeholder="Mobile Number" value="" data-msg="Please enter your first name" />
            </div>
            <div class="col-12 col-md-6">
              <label class="form-label" for="modalEditUserLastName">Physical Address</label>
               <input type="text"id="modalEditUserFirstName" name="physical_address" class="form-control" placeholder="Physical Address" value="" data-msg="Please enter your first name" />
            </div>



            <h4>Next of Kin Information</h4>

            <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditUserEmail">Names</label>
                <input type="text"id="modalEditUserFirstName" name="kin_names" class="form-control" placeholder="Names" value="" data-msg="Please enter your first name" />
            </div>
             <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditUserStatus">Relationship</label>
                <input type="text"id="modalEditUserFirstName" name="kin_relationship" class="form-control" placeholder="Relationship" value="" data-msg="Please enter your first name" />
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditTaxID">Address</label>
                <input type="text" id="modalEditTaxID" name="kin_address" class="form-control modal-edit-tax-id" placeholder="Address" value="" />
            </div>
          <div class="col-12 col-md-6">
            <label class="form-label" for="modalEditUserPhone">Contact</label>
            <input type="text" id="modalEditUserPhone" name="kin_contact"  class="form-control phone-number-mask" placeholder="Contact" value="" />
          </div>
           <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditUserEmail">Security Question</label>
                <input type="text"id="modalEditUserFirstName" name="kin_security_question" class="form-control" placeholder="Names" value="" data-msg="Please enter your first name" />
            </div>
             <div class="col-12 col-md-6">
                <label class="form-label" for="modalEditUserStatus">Answer</label>
                <input type="text"id="modalEditUserFirstName" name="kin_answer" class="form-control" placeholder="Answer" value="" data-msg="Please enter your first name" />
            </div>
          <div class="col-12 text-center mt-2 pt-50">
            <button type="submit" class="btn btn-primary me-1">Submit</button>
            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
              Discard
            </button>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
<!-- / add authentication apps modal-->

<!-- add joint sms modal-->
<div class="modal fade" id="twoFactorAuthSmsModal" tabindex="-1" aria-labelledby="twoFactorAuthSmsTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg two-factor-auth-sms modal-dialog-scrollable">
    <div class="modal-content">
        <div class="modal-header bg-transparent">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body pb-5 px-sm-5 mx-50">
            <h1 class="text-center mb-2 pb-50" id="twoFactorAuthAppsTitle">Joint Account Opening</h1>

            <h4>Account Holder One</h4>

            <form id="editUserForm" class="row gy-1 pt-75"  method="POST" action="{{ route('customers.store') }}">
                {{ csrf_field() }}
                <input hidden="" type="text" name="account_type" value="2" />
                <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">Full Names</label>
                    <input type="text"id="modalFirstName" name="full_names_one" class="form-control" placeholder="Full Names" value="" data-msg="Please enter your first name" />
                </div>
                <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">Gender</label>
                    <select id="modalEditUserStatus" name="gender_one" class="form-select" aria-label="Default select example">
                        <option selected>Gender</option>
                        <option value="Female">Female</option>
                        <option value="Male">Male</option>
                    </select>
                </div>
                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">National ID/Passport</label>
                   <input type="text"id="modalEditUserFirstName" name="national_id_one" class="form-control" placeholder="Nationa ID/Passport" value="" data-msg="Please enter your first name" />
                </div>


                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Date of Birth</label>
                   <input type="date" id="modalEditUserFirstName" name="date_of_birth_one" class="form-control" placeholder="Date of Birth" value="" data-msg="Please enter your first name" />
                </div>

                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserFirstName">Mobile Number</label>
                  <input type="text"id="modalEditUserFirstName" name="mobile_number_one" class="form-control phone-number-mask" placeholder="Mobile Number" value="" data-msg="Please enter your first name" />
                </div>
                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Physical Address</label>
                   <input type="text"id="modalEditUserFirstName" name="physical_address_one" class="form-control" placeholder="Physical Address" value="" data-msg="Please enter your first name" />
                </div>



                <h4>Account Holder Two</h4>

                 <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">Full Names</label>
                    <input type="text"id="modalFirstName" name="full_names_two" class="form-control" placeholder="Full Names" value="" data-msg="Please enter your first name" />
                </div>
                <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">Gender</label>
                    <select id="modalEditUserStatus" name="gender_two" class="form-select" aria-label="Default select example">
                        <option selected>Gender</option>
                        <option value="Female">Female</option>
                        <option value="Male">Male</option>
                    </select>
                </div>
                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">National ID/Passport</label>
                   <input type="text"id="modalEditUserFirstName" name="national_id_two" class="form-control" placeholder="Nationa ID/Passport" value="" data-msg="Please enter your first name" />
                </div>


                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Date of Birth</label>
                   <input type="date" id="modalEditUserFirstName" name="date_of_birth_two" class="form-control" placeholder="Date of Birth" value="" data-msg="Please enter your first name" />
                </div>

                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserFirstName">Mobile Number</label>
                  <input type="text"id="modalEditUserFirstName" name="mobile_number_two" class="form-control phone-number-mask" placeholder="Mobile Number" value="" data-msg="Please enter your first name" />
                </div>
                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Physical Address</label>
                   <input type="text"id="modalEditUserFirstName" name="physical_address_two" class="form-control" placeholder="Physical Address" value="" data-msg="Please enter your first name" />
                </div>

                <h4>Account Information</h4>

                <div class="col-12">
                    <label class="form-label" for="modalEditUserName">Account Names</label>
                    <input type="text" id="modalEditUserName" name="account_names" class="form-control" value="" placeholder="Account Names" />
                </div>

                <div class="col-12 text-center mt-2 pt-50">
                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                      Discard
                    </button>
                </div>
            </form>

        </div>
    </div>
  </div>
</div>
<!-- / add authentication sms modal-->

<!-- add group sms modal-->
<div class="modal fade" id="twoFactorAuthGroupsModal" tabindex="-1" aria-labelledby="twoFactorAuthSmsTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg two-factor-auth-sms modal-dialog-scrollable">
    <div class="modal-content">
        <div class="modal-header bg-transparent">
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body pb-5 px-sm-5 mx-50">
            <h1 class="text-center mb-2 pb-50" id="twoFactorAuthAppsTitle">Group Account Opening</h1>

            <form id="editUserForm" class="row gy-1 pt-75" method="POST" action="{{ route('customers.store') }}">
                {{ csrf_field() }}
                <input hidden="" type="text" name="account_type" value="3" />

                <h4>Group Account Details</h4>

                <div class="col-12">
                    <label class="form-label" for="modalEditUserFirstName">Short Description About the Group</label>
                    <input type="text"id="modalFirstName" name="description" class="form-control" placeholder="Short Description About the Group" value="" data-msg="Please enter your first name" />
                </div>
                <div class="col-12">
                  <label class="form-label" for="modalEditUserLastName">Physical Address</label>
                   <input type="text"id="modalEditUserFirstName" name="physical_address" class="form-control" placeholder="Physical Address" value="" data-msg="Please enter your first name" />
                </div>
                <div class="col-12">
                    <label class="form-label" for="modalEditUserFirstName">Group Members</label>
                    <select id="modalgroupmembers" name="group_members[]" class="select2 form-select" multiple>
                        @foreach($clients as $data)
                            <option value={{ $data->id }}> {{ $data->account_names }} </option>
                        @endforeach
                    </select>
                </div>

                <h4>Account Information</h4>

                <div class="col-12">
                    <label class="form-label" for="modalEditUserName">Account Names</label>
                    <input type="text" id="modalEditUserName" name="account_names" class="form-control" value="" placeholder="Account Names" />
                </div>


                <div class="col-12 text-center mt-2 pt-50">
                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                      Discard
                    </button>
                </div>
            </form>

        </div>
    </div>
  </div>
</div>
<!-- / add authentication sms modal-->


<!-- add business sms modal-->
<div class="modal fade" id="twoFactorAuthBusModal" tabindex="-1" aria-labelledby="twoFactorAuthSmsTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg two-factor-auth-sms modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header bg-transparent">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pb-5 px-sm-5 mx-50">
        <h1 class="text-center mb-2 pb-50" id="twoFactorAuthSmsTitle">Business Account Opening</h1>

            <form id="editUserForm" class="row gy-1 pt-75" method="POST" action="{{ route('customers.store') }}">
                {{ csrf_field() }}
                <h4>Business Account Details</h4>
                <input hidden="" type="text" name="account_type" value="4" />

                <div class="col-12">
                    <label class="form-label" for="modalEditUserFirstName">Details of Nature of Business / Sector</label>
                    <input type="text"id="modalFirstName" name="name_of_business" class="form-control" placeholder="Details of Nature of Business / Sector" value="" data-msg="Please enter your first name" />
                </div>
                <div class="col-12">
                  <label class="form-label" for="modalEditUserLastName">Certificate of Registration/ Incorporation</label>
                   <input type="text"id="modalEditUserFirstName" name="certification" class="form-control" placeholder="Certificate of Registration/ Incorporations" value="" data-msg="Please enter your first name" />
                </div>

                <div class="col-12 ">
                    <label class="form-label" for="modalEditUserFirstName">Short Description About the Business</label>
                    <input type="text"id="modalFirstName" name="description" class="form-control" placeholder="Short Description About the Group" value="" data-msg="Please enter your first name" />
                </div>


                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Office Tel.No/ Mobile No.</label>
                   <input type="text"id="modalEditUserFirstName" name="mobile_number" class="form-control" placeholder="Office Tel.No/ Mobile No." value="" data-msg="Please enter your first name" />
                </div>

                <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">Email Address</label>
                    <input type="text"id="modalFirstName" name="email_address" class="form-control" placeholder="Email Address" value="" data-msg="Please enter your first name" />
                </div>

                <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">T.I.N (If Any)</label>
                    <input type="text"id="modalFirstName" name="tin" class="form-control" placeholder="T.I.N (If Any)" value="" data-msg="Please enter your first name" />
                </div>
                <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Business /Institution Location/ (Shopping Center)</label>
                   <input type="text"id="modalEditUserFirstName" name="physical_address" class="form-control" placeholder="Business /Institution Location/ (Town / Shopping Center)" value="" data-msg="Please enter your first name" />
                </div>


                <h4>Account Signatory(s)</h4>

                 <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Signatory One</label>
                   <input type="text"id="modalEditUserFirstName" name="signatory_one" class="form-control" placeholder="Signatory One" value="" data-msg="Please enter your first name" />
                </div>

                <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">National ID</label>
                    <input type="text"id="modalFirstName" name="national_id_one" class="form-control" placeholder="National ID" value="" data-msg="Please enter your first name" />
                </div>

                 <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Signatory Two</label>
                   <input type="text"id="modalEditUserFirstName" name="signatory_two" class="form-control" placeholder="Signatory Two" value="" data-msg="Please enter your first name" />
                </div>

                <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">National ID</label>
                    <input type="text"id="modalFirstName" name="national_id_two" class="form-control" placeholder="National ID" value="" data-msg="Please enter your first name" />
                </div>

                 <div class="col-12 col-md-6">
                  <label class="form-label" for="modalEditUserLastName">Signatory Three</label>
                   <input type="text"id="modalEditUserFirstName" name="signatory_three" class="form-control" placeholder="Signatory Three" value="" data-msg="Please enter your first name" />
                </div>

                <div class="col-12 col-md-6">
                    <label class="form-label" for="modalEditUserFirstName">National ID</label>
                    <input type="text"id="modalFirstName" name="national_id_three" class="form-control" placeholder="National ID" value="" data-msg="Please enter your first name" />
                </div>


                <h4>Account Information</h4>

                <div class="col-12">
                    <label class="form-label" for="modalEditUserName">Account Names</label>
                    <input type="text" id="modalEditUserName" name="account_names" class="form-control" value="" placeholder="Account Names" />
                </div>


                <div class="col-12 text-center mt-2 pt-50">
                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                      Discard
                    </button>
                </div>
            </form>

      </div>
    </div>
  </div>
</div>
<!-- / add authentication sms modal-->
