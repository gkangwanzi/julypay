
@extends('account.index')
@section('report')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-info">
                <div class="card-body">
                    {!! Form::open(array('route' => ['modify_password', $data->id],'method'=>'PATCH')) !!}
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">
                                    <div class="mb-3">
                                        <label>New Password</label>
                                        <input type="password" class="form-control" value="" name="password" placeholder="Enter Password" required="required">
                                    </div>
                                    <div class="mb-3">
                                        <label>Confirm Password</label>
                                        <input type="password" name="confirm-password" class="form-control" value="" placeholder="Confirm Password" required="required">
                                    </div>

                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="mb-3"><br>
                            <button name="btn_submit" type="submit" class="btn btn-primary btn-block">Save Changes</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
