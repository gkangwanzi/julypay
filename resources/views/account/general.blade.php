
@extends('account.index')
@section('report')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-info">
                <div class="card-body">
                    {!! Form::open(array('route' => ['modify_general', $data->id],'method'=>'PATCH')) !!}
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">
                                    <div class="mb-3">
                                        <label>Username</label>
                                        <input type="text" class="form-control" value="{{ $data->username }}" name="username" placeholder="Enter Username" required="required">
                                    </div>
                                    <div class="mb-3">
                                        <label>Names</label>
                                        <input type="text" name="names" class="form-control" value="{{ $data->name }}" placeholder="Enter Names" required="required">
                                    </div>

                                    <div class="mb-3">
                                        <label>Country</label>
                                        <select class="form-control" name="country">
                                            <option selected>Uganda</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label>Phone Number</label>
                                        <input type="text" name="phone" class="form-control" value="{{ $data->phone }}" placeholder="Enter Phone Number (e.g 2567780123456)" required="required">
                                    </div>

                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-10 col-xs-10">
                                    <div class="mb-3">
                                        <label>Registration Date</label>
                                        <input disabled type="text" class="form-control" value="{{ $data->created_at }}" name="username" placeholder="Enter Date" required="required">
                                    </div>
                                    <div class="mb-3">
                                        <label>Email Address:</label>
                                        <input type="email" name="email" class="form-control" value="{{ $data->email }}" placeholder="Enter Email" required="required">
                                    </div>
                                    <div class="mb-3">
                                        <label>Business Name</label>
                                        <input type="text" name="business" class="form-control" value="{{ $data->business_name }}" placeholder="Enter Business Name" required="required">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="mb-3"><br>
                            <button name="btn_submit" type="submit" class="btn btn-primary btn-block">Save Changes</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
