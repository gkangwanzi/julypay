<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\User;
use App\Models\Withdraw;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use YoAPI;

class ApiController extends Controller
{
    public string $YOusername = "100515241441";
    public string $YOpassword = "TrMF-MVCZ-zVJm-N4ay-k2PW-49dJ-mCue-LOCP";


    public function getClient($phone, $amount, $trasactionID) {

        $deposit = Deposit::where(["phone" => $phone])->orderBy('id', 'DESC')->first();
        $deposit->transaction_id = $trasactionID;
        $deposit->save();
        $user = User::where(["id" => $deposit->user_id])->first();
        $user->balance = $user->balance + $amount;
        $user->save();

        $responses = Http::post($user->callback, ['apipaymentreceipt' => '1', 'msisdn' => $phone, 'amount' => $amount, 'transactionreference' => $deposit->reference, 'transactionid' => $trasactionID]);

//        $client->request('POST', $user->callback, ['form_params' => $postInput]);
        $responseBody = json_decode($responses->getBody(), true);
    }

    public function initiate_deposit(Request $request) {
            $apiKey = $request->input('apikey');
            $phone = $request->input('phone');
            $amount = $request->input('amount');
            $yoAPI = new YoAPI($this->YOusername, $this->YOpassword);
            $transaction_reference = date("YmdHis").rand(1,100);
            $yoAPI->set_external_reference($transaction_reference);
            $yoAPI->set_nonblocking("TRUE");

            $yoAPI->set_instant_notification_url('https://julypay.net/admin/api/getinstantnotification');
            $yoAPI->set_failure_notification_url('https://julypay.net/admin/api/getfailurenotification');

            $user = User::where(["api" => $apiKey])->first();
            if($user){
                $response = $yoAPI->ac_deposit_funds($phone, $amount, 'UCASH');
                $userdata["result3"] = $response;
                if($response['Status']=='OK'){
                    $init = new Deposit();
                    $init->user_id = $user->id;
                    $init->phone = $phone;
                    $init->amount = $amount;
                    $init->reference = $user->business_name."  -----  ".$user->callback;
                    $init->transaction_reference = $response['TransactionReference'];
                    $init->save();

                    $userdata["result"] = "success";

                    $responses = Http::post($user->callback,  ['apipaymentreceipt' => '2', 'msisdn' => $phone, 'amount' => $amount, 'transactionreference' => $response['TransactionReference']]);
                    json_decode($responses->getBody(), true);

                }else{
                    $userdata["result"] = "failed";
                }
            }else{
                $userdata["result"] = "failed";
            }

            echo json_encode($userdata);
    }
    public function request_relworx(Request $request) {
        $apiKey = $request->input('apikey');
        $phone = $request->input('phone');
        $amount = $request->input('amount');


        $user = User::where(["api" => $apiKey])->first();
        $cd = self::code();
        $post = array('account_no'=>'REL4AC67E0B56', 'reference' => $cd, 'msisdn' => "+".$phone, 'currency' => 'UGX', 'amount' => $amount, 'description' => $user->name);
//        $post = array('account_no'=>'RELE8D6DDC408', 'reference' => $cd, 'msisdn' => $phone, 'currency' => 'UGX', 'amount' => $amount, 'description' => $user->name); // Test

        $postdata = json_encode($post);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://payments.relworx.com/api/mobile-money/request-payment");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        $headers = [
            'Content-Type: application/json',
            'Accept: application/vnd.relworx.v2',
            'Authorization: Bearer 29ee82293c0db9.Xxr19cgt2YmSJv5_Vj_PWA'
        ]; //        'Authorization: Bearer d40e2b711958bd.-3Pn1nxgMzbpY_eUahckDw'

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        $response=json_decode($json ,true);
        curl_close($ch);


        if($user){
            $userdata["result3"] = $response;

            if($response['success']){
                $init = new Deposit();
                $init->user_id = $user->id;
                $init->phone = "+".$phone;
                $init->amount = $amount;
                $init->reference = $user->business_name."  -----  ".$user->callback;
                $init->transaction_reference = $response['internal_reference'];
                $init->save();

//                $postdatas = json_encode(['apipaymentreceipt' => '2', 'msisdn' => $phone, 'amount' => $amount, 'transactionreference' => $response['internal_reference']]);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$user->callback);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "apipaymentreceipt=2&msisdn=".$phone."&amount=".$amount."&transactionreference=".$response['internal_reference']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = curl_exec($ch);
                curl_close($ch);

//                $responses = Http::post($user->callback,  ['apipaymentreceipt' => '2', 'msisdn' => $phone, 'amount' => $amount, 'transactionreference' => $response['internal_reference']]);
//                json_decode($responses->getBody(), true);

                $userdata["result"] = $phone."  success ". $server_output;
//
//
            }else{
                $userdata["result"] = "failed";
            }
        }else{
            $userdata["result"] = "failed";
        }

        echo json_encode($userdata);
    }
    public function withdraw_relworx(Request $request) {
        $apiKey = $request->input('apikey');
        $phone = "+".$request->input('phone');
        $amount = $request->input('amount');


        $user = User::where(["api" => $apiKey])->first();
        $cd = self::code();
//        $post = array('account_no'=>'REL4AC67E0B56', 'reference' => $cd, 'msisdn' => $phone, 'currency' => 'UGX', 'amount' => $amount, 'description' => $user->name);
        $post = array('account_no'=>'REL4AC67E0B56', 'reference' => $cd, 'msisdn' => $phone, 'currency' => 'UGX', 'amount' => $amount, 'description' => $user->name);

        $postdata = json_encode($post);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://payments.relworx.com/api/mobile-money/send-payment");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        $headers = [
            'Content-Type: application/json',
            'Accept: application/vnd.relworx.v2',
            'Authorization: Bearer 29ee82293c0db9.Xxr19cgt2YmSJv5_Vj_PWA'

        ]; //        'Authorization: Bearer d40e2b711958bd.-3Pn1nxgMzbpY_eUahckDw'

        //  'Authorization: Bearer 29ee82293c0db9.Xxr19cgt2YmSJv5_Vj_PWA'

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        $response=json_decode($json ,true);
        curl_close($ch);




        return $response;
    }

    public function initiate_relworx(Request $request){

        $phone = $request->input('msisdn');
        $trasactionID = $request->input('customer_reference');
        $trac = $request->input('internal_reference');
        $amount = $request->input('amount');

        if($request->input("status") == "success"){

            $deposit = Deposit::where(["transaction_reference" => $trac])->orderBy('id', 'DESC')->first();
            $deposit->transaction_id = $trasactionID;
            $deposit->save();
            $user = User::where(["id" => $deposit->user_id])->first();
            $user->balance = $user->balance + $amount;
            $user->save();

//        $responses = Http::post($user->callback, ['apipaymentreceipt' => '1', 'msisdn' => $phone, 'amount' => $amount, 'transactionreference' => $deposit->reference, 'transactionid' => $trasactionID]);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$user->callback);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "apipaymentreceipt=1&msisdn=".$phone."&amount=".$amount."&transactionreference=".$deposit->reference."&transactionid=".$trasactionID);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close($ch);
//        $client->request('POST', $user->callback, ['form_params' => $postInput]);
//        $responseBody = json_decode($responses->getBody(), true);

        }
    }

    public function withdrawcharges($withdrawchages){
        if ($withdrawchages <= 500) {$chargeamt=250;}
        if ($withdrawchages > 501 && $withdrawchages <= 5000) {$chargeamt=250;}
        if ($withdrawchages > 5001 && $withdrawchages <= 30000) {$chargeamt=500;}
        if ($withdrawchages > 30001 && $withdrawchages <= 100000) {$chargeamt=600;}
        if ($withdrawchages > 100001 && $withdrawchages <= 500000) {$chargeamt=1000;}
        if ($withdrawchages > 500001 && $withdrawchages <= 4000000) {$chargeamt=1100;}
        if ($withdrawchages > 4000001 && $withdrawchages <= 8000000) {$chargeamt=2200;}
        if ($withdrawchages > 8000001 && $withdrawchages <= 20000000) {$chargeamt=4400;}
        if ($withdrawchages > 20000001 && $withdrawchages <= 50000000) {$chargeamt=8800;}
        if ($withdrawchages > 50000001 && $withdrawchages <= 200000000) {$chargeamt=10000;}
        if ($withdrawchages >= 200000001) {$chargeamt=11000;}

        return $chargeamt;
    }

    /**
     * @throws \Exception
     */
    public function WithdrawFundsInitiationQuery(Request $request) {

        $apiKey = $request->input('apikey');
        $phone = $request->input('phone');
        $amount = $request->input('amount');
        $private_key_file_location = "julypay.pem";
        $yoAPI = new YoAPI($this->YOusername, $this->YOpassword);

        $users = User::where(["api" => $apiKey])->first();
        if($users->balance < ($amount + self::withdrawcharges($amount))){
            $message["message"] = "\n However Insufficient Balance.";
            echo json_encode($message);
            $responses = Http::post($users->callback,  ['apipaymentreceipt' => '4', 'msisdn' => $phone, 'amount' => $users->balance, "message"=>"Insufficient Balance.", "JULYPAY" => "fail"]);
        }else{



            $ubal = $yoAPI->ac_acct_balance();
            $arr = $ubal["balance"];
            foreach ($arr as $event) {
                if($event["code"]=="UGX-MTNMM"){ $mtnbal = $event["balance"];}
                if($event["code"]=="UGX-WTLMM"){ $airtelbal = $event["balance"];}
                $phone1 = substr($phone, 0, 5);
            }

            $mtn = array("25677", "25678", "25676");
            $airtel = array("25675", "25670", "25674");

//            var_dump("AMT MTN: ".$mtnbal." AMT AIRTEL: ".$airtelbal);

            if (in_array($phone1, $mtn)){
                if($amount > $mtnbal){
                    $message["message"] = "You can only withdraw UGX ". number_format(($mtnbal-2000))." on this MTN";
                    $responses = Http::post($users->callback,  ['apipaymentreceipt' => '4', 'msisdn' => $phone, 'amount' => $users->balance, "message" => "You can only withdraw UGX ". number_format(($mtnbal-2000))." on this MTN", "JULYPAY" => "fail"]);
                    echo response()->json($message, 200);
                    echo $message;
               }else{
                    $yoAPI->set_external_reference(date("YmdHis").rand(1,100));
                    $yoAPI->set_private_key_file_location($private_key_file_location);
                    $yoAPI->set_public_key_authentication_nonce(date("YmdHis").rand(1,100));
                    $yoAPI->generate_public_key_authentication_signature($phone, $amount, "JULYPAY");
                    $trac = $yoAPI->ac_withdraw_funds($phone, $amount,"JULYPAY");
                    $this->withdraw_funds_db_insert($users, $phone, $amount, $trac);
//                    echo json_encode($trac);
                    echo response()->json($trac, 200);
                    echo $trac;
                }
            }elseif (in_array($phone1, $airtel)){
                if($amount > $airtelbal){
                    $message["message"] = "You can only withdraw UGX ". number_format(($airtelbal-2000))." on this Airtel";
                    echo json_encode($message);
                    $responses = Http::post($users->callback,  ['apipaymentreceipt' => '4', 'msisdn' => $phone, 'amount' => $users->balance, "message" => "You can only withdraw UGX ". number_format(($airtelbal-2000))." on this Airtel", "JULYPAY" => "fail"]);
                }else{
                    $yoAPI->set_external_reference(date("YmdHis").rand(1,100));
                    $yoAPI->set_private_key_file_location($private_key_file_location);
                    $yoAPI->set_public_key_authentication_nonce(date("YmdHis").rand(1,100));
                    $yoAPI->generate_public_key_authentication_signature($phone, $amount, "JULYPAY");
                    $trac = $yoAPI->ac_withdraw_funds($phone, $amount,"JULY");

                    $this->withdraw_funds_db_insert($users, $phone, $amount, $trac);
                    echo response()->json($trac, 200);
                    echo $trac;
                }
            }
        }
    }

    static function random_alphanumeric_string($length) {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($chars), 0, $length);
    }

    public function getinstantnotification(){
        $yoAPI = new YoAPI($this->YOusername, $this->YOpassword);
        $response = $yoAPI->receive_payment_notification();

        $amount = $response['amount'];
        $msisdn = $response['msisdn'];
        $narrative = $response['narrative'];
        $extenal = $response['network_ref'];
        $transactionID = $response['network_ref'];
        $this->getClient($msisdn, $amount, $transactionID);
    }

    public function getfailurenotification()
    {
        $yoAPI = new YoAPI($this->YOusername, $this->YOpassword);
        $response = $yoAPI->receive_payment_failure_notification();
        if($response['is_verified']){
            // Notification is from Yo! Uganda Limited
            echo "Failed Transaction Details: \n";
            echo "FAILED TRANSACTION REFERENCE: " . $response['failed_transaction_reference'] . "\n";
            echo "TRANSACTION INITIATION DATE: " . $response['transaction_init_date'] . "\n";
        }
    }

    /**
     * @param $users
     * @param mixed $phone
     * @param mixed $amount
     * @param array $trac
     * @return array|null
     */
    public function withdraw_funds_db_insert($users, mixed $phone, mixed $amount, array $trac): ? array
    {
        $with = new Withdraw();
        $with->user_id = $users->id;
        $with->phone = $phone;
        $with->amount = $amount;
        $with->charge = self::withdrawcharges($amount);
        $with->transaction_id = $trac['MNOTransactionReferenceId'];
        $with->transaction_reference = $trac['TransactionReference'];
        $with->save();

        $users->balance = $users->balance - ($amount + self::withdrawcharges($amount));
        $users->save();

        $responses = Http::post($users->callback, ['apipaymentreceipt' => '3', 'msisdn' => $phone, 'amount' => $amount, 'transactionreference' => $trac['TransactionReference'], 'transactionid' => $trac['MNOTransactionReferenceId'], "charge" => self::withdrawcharges($amount)]);
        $message["message"] = "success";
        $message["data"] = $trac;
        $message["charges"] = self::withdrawcharges($amount);
        $message["tracid"] = $trac['TransactionReference'];
        echo json_encode($message);
        echo response()->json($message, 200);
//        echo $message;
        return null;
    }

//    public static function code(){
//        $characters = 'abcdefghijklmnopqrstuvwxyz';
//        $pin = mt_rand(1000000000000000000, 99999999999999999999) . mt_rand(1000000000000000000, 99999999999999999999) . $characters[rand(0, strlen($characters) - 1)];
//        $string = str_shuffle($pin);
//        return $string;
//    }
    public static function code(){
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        $pin = mt_rand(10000000000, 99999999999) . mt_rand(10000000000, 99999999999) . $characters[rand(0, strlen($characters) - 1)];
        $string = str_shuffle($pin);
        return $string;
    }
}
