<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function general_setting()
    {
        $data = User::find(auth()->id());
        return view('account.general', compact('data'));
    }
    public function change_password()
    {
        $data = User::find(auth()->id());
        return view('account.password', compact('data'));
    }

    public function modify_general(Request $request, string $id)
    {
        $fm = User::findorfail($id);
        $fm->username = strip_tags($request->input('username'));
        $fm->name = strip_tags($request->input('names'));
        $fm->email = strip_tags($request->input('email'));
        $fm->business_name = strip_tags($request->input('business'));
        $fm->phone = strip_tags($request->input('phone'));
        $fm->save();

        return redirect()->route('general_setting')->with('success','General Settings Modified Successfully');
    }
    public function modify_password(Request $request, string $id)
    {
        $this->validate($request, [
            'password'   => 'required|same:confirm-password',
        ]);

        $fm = User::findorfail($id);
        $fm->password = Hash::make($request->input('password'));
        $fm->save();

        return redirect()->route('general_setting')->with('success','Password Modified Successfully');
    }
}
