<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\country;
use App\Models\HospitalImages;
use App\Models\Hospitals;
use App\Models\ImageLabelling;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        return view('home');
    }
}
