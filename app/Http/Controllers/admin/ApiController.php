<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    public function api_keys()
    {
        $data = User::find(auth()->id());
        return view('api.keys', compact('data'));
    }
    public function call_back()
    {
        $data = User::find(auth()->id());
        return view('api.callback', compact('data'));
    }

    public function modify_key(Request $request, string $id)
    {
        $fm = User::findorfail($id);
        $fm->api = self::random_alphanumeric_string(40);
        $fm->public_key = "PUK-". self::random_alphanumeric_string(20);
        $fm->save();

        return redirect()->route('api_keys')->with('success','API Keys Modified Successfully');
    }
    public function modify_callback(Request $request, string $id)
    {
        $fm = User::findorfail($id);
        $fm->callback = strip_tags($request->input('callback'));
        $fm->save();

        return redirect()->route('call_back')->with('success','Callback Url Modified Successfully');
    }

    static function random_alphanumeric_string($length) {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($chars), 0, $length);
    }
}
