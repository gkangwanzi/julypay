<?php

namespace App\Http\Controllers\admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use YoAPI;

class DepositController extends Controller
{
    public string $YOusername = "100515241441";
    public string $YOpassword = "TrMF-MVCZ-zVJm-N4ay-k2PW-49dJ-mCue-LOCP";
    public function index()
    {
        $member = Deposit::where(["user_id" => auth()->id()])->orderBy('id', 'DESC')->get();
        $user = User::where(["id" => auth()->id()])->first();
        return view('deposits.index', compact("member", "user"));
    }

    public function store(Request $request)
    {
        $phone= $request->input('phone');
        $amount = $request->input('amount');

        $yoAPI = new YoAPI($this->YOusername, $this->YOpassword);
        $transaction_reference = date("YmdHis").rand(1,100);
        $yoAPI->set_external_reference($transaction_reference);
        $yoAPI->set_nonblocking("TRUE");

        $yoAPI->set_instant_notification_url('https://pay.julyhost.net/admin/api/getinstantnotification');
        $yoAPI->set_failure_notification_url('https://pay.julyhost.net/admin/api/getfailurenotification');

        $user = User::where(["id" => auth()->id()])->first();
        if($user){
            $response = $yoAPI->ac_deposit_funds($phone, $amount, 'UCASH');
            if($response['Status']=='OK'){
                $init = new Deposit();
                $init->user_id = $user->id;
                $init->phone = $phone;
                $init->amount = $amount;
                $init->reference = $user->business_name;
                $init->transaction_reference = $response['TransactionReference'];
                $init->save();

                $ch = curl_init($user->callback);  // Create a new cURL resource
                $data = array(
                    'msdn' => $phone,
                    'amount' => $amount,
                    'transactionreference' => $response['TransactionReference']
                );
                $payload = json_encode($data); // Setup request to send json via POST
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload); // Attach encoded JSON string to the POST fields
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));  // Set the content type to application/json
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  // Return response instead of outputting
                $result = curl_exec($ch); // Execute the POST request
                curl_close($ch); // Close cURL resource

                return redirect()->route('deposits.index')->with('success','Initiated Successfully');
            }else{
                return redirect()->route('deposits.index')->with('success','Transaction Failed!');
            }
        }


    }
    public function deposits_check(Request $request)
    {
        $request->validate([
            'member_account' => 'required',
        ]);
    }


    public function withdraw(Request $request)
    {
        $amount = $request->input('amount_withdrawn');
        $clientID = $request->input('id');

        $client = Client::where('id',$clientID)->first();
        $balance = $client->saving_balance - $amount;
        Client::where('id', $clientID)->update(['saving_balance' => $balance]);

        $withdraw = new CustomerTransaction();
        $withdraw->client_id = $clientID;
        $withdraw->amount = $amount;
        $withdraw->saving = $balance;
        $withdraw->shares = $client->shares_balance;
        $withdraw->loan = $client->loan_balance;
        $withdraw->interest = $client->loan_interest;
        $withdraw->handler = $request->input('withdrawor');
        $withdraw->transaction_type = "2";
        $withdraw->created_by = auth()->id();
        $withdraw->updated_by = auth()->id();
        $withdraw->save();
    }

    public function show($id)
    {
        $breadcrumbs = [['link' => "#", 'name' => Carbon::now()->timezone('Africa/Nairobi')->toDayDateTimeString()]];
        $clientlist = Client::orderBy('id', 'ASC')->get();
        $depositoptions = DepositOption::orderBy('id', 'ASC')->get();
        $memberId = Helper::split_on_array($id, 1)[1];
        $clients = Client::where('id', $memberId)->first();
        $transactions = CustomerTransaction::where('client_id', $memberId)->orderBy('id', 'DESC')->get();
        return view('deposits.show', compact('clients','clientlist', 'depositoptions', 'memberId', 'transactions', 'breadcrumbs'));

//        $clients = Client::all();
//        $memberId = Helper::split_on_array($id, 1)[1];
//        return view('deposits.show', compact("clients", "memberId"));
    }

    public function deposit_categories($id)
    {
        $clientlist = Client::orderBy('id', 'ASC')->get();
        $depositoptions = DepositOption::orderBy('id', 'ASC')->get();
        $data = Helper::split_on_array($id, 1);
        $selectedID = $data[1];
        $clients = Client::where('id', $data[1])->first();
        return view('deposits.options', compact('clients','clientlist', 'depositoptions', 'selectedID'));
    }


}
