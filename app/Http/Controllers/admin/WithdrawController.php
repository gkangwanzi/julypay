<?php

namespace App\Http\Controllers\admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Club;
use App\Models\CustomerTransaction;
use App\Models\Deposit;
use App\Models\DepositOption;
use App\Models\Farmer;
use App\Models\LoanApplication;
use App\Models\LoanData;
use App\Models\User;
use App\Models\Withdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;
use YoAPI;

class WithdrawController extends Controller
{
    public function index()
    {
        $member = Withdraw::where(["user_id" => auth()->id()])->orderBy('id', 'DESC')->get();
        $user = User::where(["id" => auth()->id()])->first();
        return view('withdraws.index', compact("member", "user"));
    }

    public function store(Request $request)
    {

        $users = User::findOrfail(auth()->id());
        if($users->amount < $request->input("amount")){
            return back()->with('warning','Insufficient Funds');
        }else{
            $data = Http::post('https://julypay.net/admin/api/withdraw', ['apikey' => $users->api, 'phone' => $request->input("phone"), 'amount' => $request->input("amount")]);
            $var = json_decode($data, true);

            return back()->with('success','Successfully Initiated Withdraw Request  '.$var);
        }
    }
    public function deposits_check(Request $request)
    {
        $request->validate([
            'member_account' => 'required',
        ]);
    }


    public function withdraw(Request $request)
    {
        $amount = $request->input('amount_withdrawn');
        $clientID = $request->input('id');

        $client = Client::where('id',$clientID)->first();
        $balance = $client->saving_balance - $amount;
        Client::where('id', $clientID)->update(['saving_balance' => $balance]);

        $withdraw = new CustomerTransaction();
        $withdraw->client_id = $clientID;
        $withdraw->amount = $amount;
        $withdraw->saving = $balance;
        $withdraw->shares = $client->shares_balance;
        $withdraw->loan = $client->loan_balance;
        $withdraw->interest = $client->loan_interest;
        $withdraw->handler = $request->input('withdrawor');
        $withdraw->transaction_type = "2";
        $withdraw->created_by = auth()->id();
        $withdraw->updated_by = auth()->id();
        $withdraw->save();
    }

    public function show($id)
    {
        $breadcrumbs = [['link' => "#", 'name' => Carbon::now()->timezone('Africa/Nairobi')->toDayDateTimeString()]];
        $clientlist = Client::orderBy('id', 'ASC')->get();
        $depositoptions = DepositOption::orderBy('id', 'ASC')->get();
        $memberId = Helper::split_on_array($id, 1)[1];
        $clients = Client::where('id', $memberId)->first();
        $transactions = CustomerTransaction::where('client_id', $memberId)->orderBy('id', 'DESC')->get();
        return view('deposits.show', compact('clients','clientlist', 'depositoptions', 'memberId', 'transactions', 'breadcrumbs'));

//        $clients = Client::all();
//        $memberId = Helper::split_on_array($id, 1)[1];
//        return view('deposits.show', compact("clients", "memberId"));
    }

    public function deposit_categories($id)
    {
        $clientlist = Client::orderBy('id', 'ASC')->get();
        $depositoptions = DepositOption::orderBy('id', 'ASC')->get();
        $data = Helper::split_on_array($id, 1);
        $selectedID = $data[1];
        $clients = Client::where('id', $data[1])->first();
        return view('deposits.options', compact('clients','clientlist', 'depositoptions', 'selectedID'));
    }


}
